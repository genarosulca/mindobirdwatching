package ec.com.mindo.mindobirdwatching.routes.description.info;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;

public class RoutesInfoHotspotAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<PoiVO> listStorage;
    private Context context;

    public RoutesInfoHotspotAdapter(Context context, List<PoiVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_route_hotspot_info, parent, false);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.route_hotspot_info_name);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getTitle());

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
    }
}
