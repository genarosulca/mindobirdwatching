package ec.com.mindo.mindobirdwatching.routes.description.info;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;

public class RoutesInfoBirdsAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<BirdVO> listStorage;
    private Context context;

    public RoutesInfoBirdsAdapter(Context context, List<BirdVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_route_bird_info, parent, false);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.route_bird_info_name);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getName());

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
    }
}
