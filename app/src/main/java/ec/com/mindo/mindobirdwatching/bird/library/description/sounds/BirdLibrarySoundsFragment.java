package ec.com.mindo.mindobirdwatching.bird.library.description.sounds;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibrarySoundsFragment extends Fragment {

    public static MediaPlayer mediaPlayer;
    public static Integer playingId;
    public static Integer totalTime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedObj = inflater.inflate(R.layout.fragment_bird_library_sounds, container, false);
        GridView gridview = inflatedObj.findViewById(R.id.audio_grid);
        BirdVO selectedBird = (BirdVO) getArguments().getSerializable(BirdLibraryDescriptionActivity.SELECTED_BIRD);
//        selectedBird.setSoundsGallery(getAllSounds(selectedBird.getBirdId()));

        BirdLibrarySoundsAdapter customAdapter = new BirdLibrarySoundsAdapter(inflatedObj.getContext(), selectedBird.getSoundsGallery());
        gridview.setAdapter(customAdapter);

        Util.hideUnavaliableButtons(getActivity(),
                selectedBird,
                getArguments().getBoolean(Constants.FROM_DISCOVER)
        );
        mediaPlayer = new MediaPlayer();
        playingId = null;
        FrameLayout header = getActivity().findViewById(R.id.bird_description_header);
        header.setVisibility(View.GONE);
        ImageView imageView = inflatedObj.findViewById(R.id.bird_sound_title_background);
        File dir = getActivity().getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = selectedBird.getImageName() + "." + selectedBird.getExt();
        File mypath = new File(dir,filename);
        imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        TextView title = inflatedObj.findViewById(R.id.bird_sound_title_txt);
        title.setText(selectedBird.getName());
        return inflatedObj;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
    }
}
