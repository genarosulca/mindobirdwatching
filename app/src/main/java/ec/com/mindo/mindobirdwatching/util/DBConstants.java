package ec.com.mindo.mindobirdwatching.util;

public class DBConstants {

    // DB names

    public static final String DB_SPANISH_NAME = "bd_mindo_birdwatching_es";
    public static final String DB_ENGLISH_NAME = "bd_mindo_birdwatching_en";
    public static final String DB_VERSION = "bd_version";

    // Types
    public static final String CREATE = "CREATE TABLE ";
    public static final String OPEN_PARENTHESIS = " (";
    public static final String CLOSE_PARENTHESIS = ")";
    public static final String INTEGER_TYPE = " INTEGER";
    public static final String TEXT_TYPE = " TEXT";
    public static final String REAL_TYPE = " REAL";
    public static final String COMMA = ", ";
    public static final String DROP = "DROP TABLE IF EXISTS ";

    // Tables
    public static final String TABLE_DBINFO = "DBINFO";
    public static final String TABLE_POI = "POI";
    public static final String TABLE_ROUTE = "ROUTE";
    public static final String TABLE_BIRD = "BIRD";
    public static final String TABLE_BIRD_IMAGE = "BIRDIMAGE";
    public static final String TABLE_BIRD_VIDEO = "BIRDVIDEO";
    public static final String TABLE_FAMILY = "FAMILY";
    public static final String TABLE_BIRD_SOUND = "SOUND";
    // IDs
    public static final String FIELD_ID = "id";
    public static final String FIELD_POI_ID = "poiId";
    public static final String FIELD_ROUTE_ID = "routeId";
    public static final String FIELD_BIRD_ID = "birdId";
    public static final String FIELD_FAMILY_ID = "familyId";
    // Fields
    public static final String FIELD_VERSION = "version";
    public static final String FIELD_IMAGE_NAME = "imageName";
    public static final String FIELD_EXT = "ext";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_SUBTITLE = "subtitle";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_IN_DANGER = "inDanger";
    public static final String FIELD_ACTIVATE_AR = "activateAr";
    public static final String FIELD_SCIENTIFIC_NAME = "scientificName";
    public static final String FIELD_LIFETIME = "lifetime";
    public static final String FIELD_SPEED = "speed";
    public static final String FIELD_WEIGHT = "weight";
    public static final String FIELD_SIZE = "size";
    public static final String FIELD_BIRD_INFO = "info";
    public static final String FIELD_DANGER_INFO = "dangerInfo";
    public static final String FIELD_VIDEO_NAME = "videoName";
    public static final String FIELD_SOUND_NAME = "clipName";
    public static final String FIELD_THUMBNAIL = "thumbnail";
    public static final String FIELD_THUMBNAIL_EXT = "thumbnailExt";
    public static final String FIELD_RELATED_BIRDS = "relatedBirds";
    public static final String FIELD_RELATED_POIS = "relatedPois";
    public static final String FIELD_AUTHOR = "authorInfo";

    public static final String FIELD_DISTANCE = "distance";
    public static final String FIELD_DISTANCE_NOTATION = "distanceNotation";

    public static final String NOT_NULL_FIELDS = "NOT NULL";

    public final static String CREATE_TABLE_POI = CREATE
            .concat(TABLE_POI)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IMAGE_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_LATITUDE).concat(REAL_TYPE).concat(COMMA)
            .concat(FIELD_LONGITUDE).concat(REAL_TYPE).concat(COMMA)
            .concat(FIELD_TITLE).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SUBTITLE).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_DESCRIPTION).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_RELATED_BIRDS).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_ROUTE = CREATE
            .concat(TABLE_ROUTE)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IMAGE_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_RELATED_POIS).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_RELATED_BIRDS).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_DISTANCE).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_DISTANCE_NOTATION).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_DESCRIPTION).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_BIRD = CREATE
            .concat(TABLE_BIRD)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_FAMILY_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IMAGE_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IN_DANGER).concat(INTEGER_TYPE).concat(COMMA)
            .concat(FIELD_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SCIENTIFIC_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_LIFETIME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SPEED).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_WEIGHT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SIZE).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_BIRD_INFO).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_DANGER_INFO).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_ACTIVATE_AR).concat(INTEGER_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_VERSION = CREATE
            .concat(TABLE_DBINFO)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_VERSION)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_BIRD_IMAGE = CREATE
            .concat(TABLE_BIRD_IMAGE)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_BIRD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IMAGE_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_THUMBNAIL).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_THUMBNAIL_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_AUTHOR).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_BIRD_VIDEO = CREATE
            .concat(TABLE_BIRD_VIDEO)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_BIRD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_VIDEO_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_THUMBNAIL).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_THUMBNAIL_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_AUTHOR).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_BIRD_SOUND = CREATE
            .concat(TABLE_BIRD_SOUND)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_BIRD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SOUND_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_AUTHOR).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String CREATE_TABLE_FAMILY = CREATE
            .concat(TABLE_FAMILY)
            .concat(OPEN_PARENTHESIS)
            .concat(FIELD_ID).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_IMAGE_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_EXT).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_NAME).concat(TEXT_TYPE).concat(COMMA)
            .concat(FIELD_SCIENTIFIC_NAME).concat(TEXT_TYPE)
            .concat(CLOSE_PARENTHESIS);

    public final static String DROP_TABLE_VERSION = DROP.concat(TABLE_DBINFO);
    public final static String DROP_TABLE_POI = DROP.concat(TABLE_POI);
    public final static String DROP_TABLE_ROUTE = DROP.concat(TABLE_ROUTE);
    public final static String DROP_TABLE_BIRD = DROP.concat(TABLE_BIRD);
    public final static String DROP_TABLE_BIRD_IMAGE = DROP.concat(TABLE_BIRD_IMAGE);
    public final static String DROP_TABLE_BIRD_VIDEO = DROP.concat(TABLE_BIRD_VIDEO);
    public final static String DROP_TABLE_FAMILY = DROP.concat(TABLE_FAMILY);
    public final static String DROP_TABLE_BIRD_SOUND = DROP.concat(TABLE_BIRD_SOUND);

    public static final String getDbName(String language){
        if (language.equals(Constants.SPANISH_LANGUAGE)){
            return DB_SPANISH_NAME;
        } else if(language.equals(Constants.ENGLISH_LANGUAGE)){
            return DB_ENGLISH_NAME;
        }
        return null;
    }
}
