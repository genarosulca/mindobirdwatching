package ec.com.mindo.mindobirdwatching.bird.library.description.videos;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;

import java.io.File;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

import static android.support.constraint.Constraints.TAG;

public class FullScreenVideoPlayer extends Activity implements OnPreparedListener {

    private VideoView videoView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_viewer);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        goFullScreen();

        MediaResourceVO videoObj = (MediaResourceVO) getIntent().getSerializableExtra("resource");

        File dir = this.getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = videoObj.getResourceName() + "." + videoObj.getExt();
        File mypath = new File(dir,filename);

//        Uri uri = Util.getUri(Constants.BIRDS_DIR,
//                videoObj.getResourceName(),
//                videoObj.getExt());
        videoView = findViewById(R.id.videoViewer);
        videoView.setOnPreparedListener(this);

        videoView.setVideoPath(mypath.getPath());
        this.customMediaController(videoView);
    }

    @Override
    public void onPrepared() {
        //Starts the video playback as soon as it is ready
        videoView.start();
    }

    private void goFullScreen() {
        Handler handler = new Handler();
        Runnable r = () -> {
            Log.i(TAG, "Entro a go full screen");
            View decorView = getWindow().getDecorView();
            Log.i(TAG, "No esta en FS");
            RelativeLayout relativeLayout = findViewById(R.id.esteId);
            relativeLayout.setFitsSystemWindows(false);

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN); // hide status bar
        };
        handler.postDelayed(r, 30);
    }

    private void customMediaController(VideoView emVideoView){
        ImageButton imageButton = emVideoView.findViewById(R.id.exomedia_controls_play_pause_btn);
        ViewGroup.LayoutParams params = imageButton.getLayoutParams();
        params.height = 180;
        params.width = 180;
        imageButton.setLayoutParams(params);
    }
}
