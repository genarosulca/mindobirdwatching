package ec.com.mindo.mindobirdwatching.poi.description.info;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;

public class PoiInfoBirdAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<BirdVO> listStorage;
    private Context context;

    public PoiInfoBirdAdapter(Context context, List<BirdVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PoiInfoBirdAdapter.ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new PoiInfoBirdAdapter.ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_poi_info, parent, false);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.poi_info_bird_name);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (PoiInfoBirdAdapter.ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getName());

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
    }
}
