package ec.com.mindo.mindobirdwatching.routes.description.info;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.WrappingGridView;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;
import ec.com.mindo.mindobirdwatching.routes.description.RoutesDescriptionActivity;
import ec.com.mindo.mindobirdwatching.util.BirdUtils;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class RoutesInfoFragment extends Fragment {

    private List<PoiVO> poiCol;
    private List<BirdVO> birdList;
    private Boolean hotspotOpened;
    private Boolean birdsOpened;
    private Boolean descriptionOpened;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedObj = inflater.inflate(R.layout.fragment_route_info, container, false);

        RouteVO routeVO = (RouteVO) getArguments().getSerializable(RoutesDescriptionActivity.SELECTED_ROUTE);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Filtrar pois
        List<String> poiIds = new ArrayList<>(Arrays.asList(routeVO.getRelatedPois().split(",")));

        poiCol = GlobalVar.getInstance().getPoiMainCol().stream()
                .filter(poiVO -> poiIds.stream().anyMatch(id -> id.equals(poiVO.getPoiId())))
                .collect(Collectors.toList());

        TextView description = inflatedObj.findViewById(R.id.route_description_descriptionTextView);
        description.setText(routeVO.getDescription());

        TextView distance = inflatedObj.findViewById(R.id.route_distance_txt);
        distance.setText(
                routeVO.getDistance()
                        .concat(" ")
                        .concat(routeVO.getDistanceNotation())
        );

        this.getBirdList(inflatedObj, routeVO);

        initDescription(inflatedObj, routeVO);
        initBirdGridView(inflatedObj);
        initHotspotGridView(inflatedObj);

        return inflatedObj;
    }

    private void initDescription(View inflatedObj, RouteVO routeVO){
        TextView description = inflatedObj.findViewById(R.id.route_description_descriptionTextView);
        description.setText(routeVO.getDescription());
        FrameLayout descriptionLabel = inflatedObj.findViewById(R.id.route_description_label_container);
        ImageView descriptionLabelIcon = inflatedObj.findViewById(R.id.route_description_icon);
        descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
        descriptionOpened = Boolean.TRUE;
        descriptionLabel.setOnClickListener(view -> {
            if(descriptionOpened){
                descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                description.setVisibility(View.GONE);
                descriptionOpened = Boolean.FALSE;
            } else {
                descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                description.setVisibility(View.VISIBLE);
                descriptionOpened = Boolean.TRUE;
            }
        });
    }

    private void initHotspotGridView(View inflatedObj) {
        WrappingGridView gridview = inflatedObj.findViewById(R.id.route_hotspot_gridview);
        RoutesInfoHotspotAdapter customAdapter = new RoutesInfoHotspotAdapter(inflatedObj.getContext(),
                poiCol);
        FrameLayout label = inflatedObj.findViewById(R.id.route_hotspot_label_container);
        ImageView icon = inflatedObj.findViewById(R.id.route_hotspot_icon);
        icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
        hotspotOpened = Boolean.TRUE;
        label.setOnClickListener(view -> {
            if (hotspotOpened) {
                icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                gridview.setVisibility(View.GONE);
                hotspotOpened = Boolean.FALSE;
            } else {
                icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                gridview.setVisibility(View.VISIBLE);
                hotspotOpened = Boolean.TRUE;
            }
        });

        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(view.getContext(), PoiDescriptionActivity.class);
            intent.putExtra("poiObject", poiCol.get(position));
            startActivity(intent);
        });
    }

    private void initBirdGridView(View inflatedObj) {
        WrappingGridView gridview = inflatedObj.findViewById(R.id.route_birds_gridview);
        RoutesInfoBirdsAdapter customAdapter = new RoutesInfoBirdsAdapter(inflatedObj.getContext(),
                birdList);
        FrameLayout label = inflatedObj.findViewById(R.id.route_birds_label_container);
        ImageView icon = inflatedObj.findViewById(R.id.route_birds_icon);
        icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
        birdsOpened = Boolean.TRUE;
        label.setOnClickListener(view -> {
            if (birdsOpened) {
                icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                gridview.setVisibility(View.GONE);
                birdsOpened = Boolean.FALSE;
            } else {
                icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                gridview.setVisibility(View.VISIBLE);
                birdsOpened = Boolean.TRUE;
            }
        });

        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(view.getContext(), BirdLibraryDescriptionActivity.class);
            BirdVO selectedBird = birdList.get(position);
            selectedBird.setSoundsGallery(BirdUtils.getAllSounds(selectedBird.getBirdId(), inflatedObj.getContext()));
            selectedBird.setVideoGallery(BirdUtils.getAllVideos(selectedBird.getBirdId(), inflatedObj.getContext()));
            selectedBird.setImageGallery(BirdUtils.getAllImages(selectedBird.getBirdId(), inflatedObj.getContext()));

            intent.putExtra("selectedBird", selectedBird);
            intent.putExtra(Constants.FROM_DISCOVER, Boolean.FALSE);
            startActivity(intent);
        });
    }

    private void getBirdList(View inflatedObj, RouteVO routeVO) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(inflatedObj.getContext(),
                DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parameters = routeVO.getRelatedBirds().split(",");
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD + " WHERE " +
                DBConstants.FIELD_ID + " IN (" + Util.makePlaceholders(parameters.length) + ")", parameters);
        birdList = new ArrayList<>();
        while (cursor.moveToNext()) {
            birdList.add(
                    new BirdVO(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getInt(4) != 0,
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getInt(13) != 0
                    )
            );
        }
        cursor.close();
        db.close();
        conn.close();
    }
}
