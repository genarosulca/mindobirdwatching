package ec.com.mindo.mindobirdwatching.language;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import java.util.Locale;

import ec.com.mindo.mindobirdwatching.MainActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.load.LoadDBActivity;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class LanguageChoseActivity extends Activity {

    private TextView englishButton;
    private TextView spanishButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_language_choose);

        getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        englishButton = this.findViewById(R.id.english_btn);
        englishButton.setOnClickListener(view -> {
            GlobalVar.getInstance().setPoiMainCol(null);
            saveLocale(Constants.ENGLISH_LANGUAGE);
            goToMain();
        });
        spanishButton = this.findViewById(R.id.spanish_btn);
        spanishButton.setOnClickListener(view -> {
            GlobalVar.getInstance().setPoiMainCol(null);
            saveLocale(Constants.SPANISH_LANGUAGE);
            goToMain();
        });
    }

    public void saveLocale(String lang) {
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.LANG_VAR, lang);
        editor.apply();

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }


    private void goToMain(){
        Intent intent = new Intent(this, LoadDBActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
