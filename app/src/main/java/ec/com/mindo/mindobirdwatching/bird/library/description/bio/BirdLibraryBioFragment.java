package ec.com.mindo.mindobirdwatching.bird.library.description.bio;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.WrappingGridView;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;
import ec.com.mindo.mindobirdwatching.routes.description.info.RoutesInfoHotspotAdapter;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryBioFragment extends Fragment {

    private Boolean dangerOpenned;
    private Boolean hotspotOpened;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedObj = inflater.inflate(R.layout.fragment_bird_library_bio, container, false);

        BirdVO selectedBird = (BirdVO) getArguments().getSerializable(BirdLibraryDescriptionActivity.SELECTED_BIRD);

        if(selectedBird.getFamilyScientificName() == null){
            ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(inflatedObj.getContext(),
                    DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            String[] parameters = {selectedBird.getFamilyId()};
            Cursor cursor = db.rawQuery("SELECT " + DBConstants.FIELD_SCIENTIFIC_NAME + " FROM " + DBConstants.TABLE_FAMILY + " WHERE " +
                    DBConstants.FIELD_ID + " = ?", parameters);
            cursor.moveToFirst();
            selectedBird.setFamilyScientificName(cursor.getString(0));
            cursor.close();
            db.close();
            conn.close();
        }

        TextView sciName = inflatedObj.findViewById(R.id.bio_sci_txt);
        sciName.setText(selectedBird.getScientificName());

        TextView familySciName = inflatedObj.findViewById(R.id.bio_fammily_sci_txt);
        familySciName.setText(selectedBird.getFamilyScientificName());

        TextView lifetime = inflatedObj.findViewById(R.id.bio_lifetime_txt);
        if(selectedBird.getLifetime() == null || selectedBird.getLifetime().equals("")){
            TextView lifetimeLabel = inflatedObj.findViewById(R.id.bio_lifetime_label);
            lifetimeLabel.setVisibility(View.GONE);
            lifetime.setVisibility(View.GONE);
        } else {
            lifetime.setText(selectedBird.getLifetime());
        }

        TextView speed = inflatedObj.findViewById(R.id.bio_speed_txt);
        if(selectedBird.getSpeed() == null || selectedBird.getSpeed().equals("")){
            TextView speedLabel = inflatedObj.findViewById(R.id.bio_speed_label);
            speedLabel.setVisibility(View.GONE);
            speed.setVisibility(View.GONE);
        } else {
            speed.setText(selectedBird.getSpeed());
        }

        TextView weight = inflatedObj.findViewById(R.id.bio_weight_txt);
        if(selectedBird.getWeight() == null || selectedBird.getWeight().equals("")){
            TextView weightLabel = inflatedObj.findViewById(R.id.bio_weight_label);
            weightLabel.setVisibility(View.GONE);
            weight.setVisibility(View.GONE);
        } else {
            weight.setText(selectedBird.getWeight());
        }

        TextView size = inflatedObj.findViewById(R.id.bio_size_txt);
        if(selectedBird.getSize() == null || selectedBird.getSize().equals("")) {
            TextView sizeLabel = inflatedObj.findViewById(R.id.bio_size_label);
            sizeLabel.setVisibility(View.GONE);
            size.setVisibility(View.GONE);
        } else {
            size.setText(selectedBird.getSize());
        }

        TextView info = inflatedObj.findViewById(R.id.bio_info);
        info.setText(selectedBird.getInfo());

        FrameLayout dangerLabel = inflatedObj.findViewById(R.id.bio_danger_label_container);
        ImageView dangerLabelIcon = inflatedObj.findViewById(R.id.bio_danger_icon);
        TextView dangerTxt = inflatedObj.findViewById(R.id.bio_danger);
        if(selectedBird.getInDanger()){
            dangerLabel.setVisibility(View.VISIBLE);
            dangerTxt.setVisibility(View.GONE);
            dangerLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
            dangerTxt.setText(selectedBird.getDangerInfo());
            dangerOpenned = Boolean.FALSE;

            dangerLabel.setOnClickListener(view -> {
                if(dangerOpenned){
                    dangerLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                    dangerTxt.setVisibility(View.GONE);
                    dangerOpenned = Boolean.FALSE;
                } else {
                    dangerLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                    dangerTxt.setVisibility(View.VISIBLE);
                    dangerOpenned = Boolean.TRUE;
                }
            });
        } else {
            dangerLabel.setVisibility(View.GONE);
            dangerTxt.setVisibility(View.GONE);
        }


        info.setText(selectedBird.getInfo());

        Util.hideUnavaliableButtons(getActivity(),
                selectedBird,
                getArguments().getBoolean(Constants.FROM_DISCOVER)
        );

        FrameLayout header = getActivity().findViewById(R.id.bird_description_header);
        header.setVisibility(View.GONE);

        ImageView imageView = inflatedObj.findViewById(R.id.bird_bio_title_background);
        File dir = getActivity().getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = selectedBird.getImageName() + "." + selectedBird.getExt();
        File mypath = new File(dir,filename);
        imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));
        imageView.setImageMatrix(Util.scaleTop(imageView, inflatedObj.getContext()));

        TextView title = inflatedObj.findViewById(R.id.bird_bio_title_text);
        title.setText(selectedBird.getName());

        initHotspotGridView(inflatedObj, selectedBird);

        return inflatedObj;
    }

    private List<PoiVO> getRelatedPois(BirdVO selectedBird){
        List<PoiVO> relatedPois = GlobalVar.getInstance().getPoiMainCol().stream()
                .filter(poiVO -> poiVO.getRelatedBirds().contains(selectedBird.getBirdId()))
                .collect(Collectors.toList());
        Log.i("ASDSAADDSA", "Numero POIs relacionados: " + relatedPois.size());
//        initHotspotGridView(inflatedObj);
        return relatedPois;
    }

    private void initHotspotGridView(View inflatedObj, BirdVO selectedBird) {
        WrappingGridView gridview = inflatedObj.findViewById(R.id.bird_hotspot_gridview);
        List<PoiVO> relatedPois = getRelatedPois(selectedBird);
        FrameLayout label = inflatedObj.findViewById(R.id.bird_hotspot_label_container);
        if(relatedPois.isEmpty()){
            label.setVisibility(View.GONE);
        } else {
            BirdHotspotAdapter customAdapter = new BirdHotspotAdapter(inflatedObj.getContext(),
                    relatedPois);

            ImageView icon = inflatedObj.findViewById(R.id.bird_hotspot_icon);
            icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
            hotspotOpened = Boolean.FALSE;
            gridview.setVisibility(View.GONE);
            label.setOnClickListener(view -> {
                if (hotspotOpened) {
                    icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                    gridview.setVisibility(View.GONE);
                    hotspotOpened = Boolean.FALSE;
                } else {
                    icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                    gridview.setVisibility(View.VISIBLE);
                    hotspotOpened = Boolean.TRUE;
                }
            });

            gridview.setAdapter(customAdapter);

            gridview.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent = new Intent(view.getContext(), PoiDescriptionActivity.class);
                intent.putExtra("poiObject", relatedPois.get(position));
                startActivity(intent);
            });
        }
    }

}
