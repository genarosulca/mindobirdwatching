package ec.com.mindo.mindobirdwatching.bird.library.description.sounds;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibrarySoundsAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<MediaResourceVO> listStorage;
    private Context context;

    public BirdLibrarySoundsAdapter(Context context, List<MediaResourceVO> customizedListView) {
        this.context = context;
        layoutinflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if (convertView == null) {
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_sound_gallery, parent, false);
            listViewHolder.playStopButton = (ImageView) convertView.findViewById(R.id.audio_play_sound_button);
            convertView.setTag(listViewHolder);
        } else {
            listViewHolder = (ViewHolder) convertView.getTag();
        }

        listViewHolder.playStopButton.setImageResource(R.drawable.ic_play_arrow_white);

        listViewHolder.playStopButton.setOnClickListener(view -> {
            if (BirdLibrarySoundsFragment.mediaPlayer.isPlaying()) {
                if(BirdLibrarySoundsFragment.playingId == position){
                    BirdLibrarySoundsFragment.mediaPlayer.stop();
                    listViewHolder.playStopButton.setImageResource(R.drawable.ic_play_arrow_white);
                }
            } else {
                playSound(position, listViewHolder);
            }
        });

        return convertView;
    }

    private void playSound(int position, ViewHolder listViewHolder){
        BirdLibrarySoundsFragment.playingId = position;
        BirdLibrarySoundsFragment.mediaPlayer = new MediaPlayer();
        File dir = context.getDir(Constants.I_SOUNDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getResourceName() + "." + listStorage.get(position).getExt();
        File file = new File(dir, filename);
        Uri uri = Uri.parse(file.getAbsolutePath());
        try {
            BirdLibrarySoundsFragment.mediaPlayer.setDataSource(context, uri);
//                    BirdLibrarySoundsFragment.mediaPlayer.setOnPreparedListener(MediaPlayer::start);
            BirdLibrarySoundsFragment.mediaPlayer.prepareAsync();
            BirdLibrarySoundsFragment.mediaPlayer.setOnPreparedListener(mediaPlayer -> {
                mediaPlayer.start();
                listViewHolder.playStopButton.setImageResource(R.drawable.ic_stop);
            });
            BirdLibrarySoundsFragment.mediaPlayer.setOnCompletionListener(mp -> listViewHolder.playStopButton.setImageResource(R.drawable.ic_play_arrow_white));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class ViewHolder {
        ImageView playStopButton;
    }
}
