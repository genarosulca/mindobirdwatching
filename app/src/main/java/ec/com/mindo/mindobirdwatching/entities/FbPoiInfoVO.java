package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbPoiInfoVO implements Serializable {

    private String language;
    private String title;
    private String subTitle;
    private String description;
}
