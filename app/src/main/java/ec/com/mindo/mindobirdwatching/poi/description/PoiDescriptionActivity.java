package ec.com.mindo.mindobirdwatching.poi.description;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.load.LoadDBActivity;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.poi.description.info.PoiInfoFragment;
import ec.com.mindo.mindobirdwatching.poi.description.map.PoiMapFragment;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class PoiDescriptionActivity extends BaseActivity {

    private static final String TAG = PoiDescriptionActivity.class.getSimpleName();
    public static final String SELECTED_POI = "selectedPoi";

    private PoiVO poiObject;

    private FrameLayout frameLayout;
    private BottomNavigationView bottomNavigationView;

    private PoiInfoFragment poiInfoFragment;
    private PoiMapFragment poiMapFragment;
    private Boolean fromDiscover = Boolean.FALSE;

    // The entry point to the Fused Location Provider.
//    private FusedLocationProviderClient mFusedLocationProviderClient;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
//    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_poi_description);
        LayoutInflater inflater = getLayoutInflater();
        fromDiscover = Boolean.FALSE;

        inflater.inflate(R.layout.activity_poi_description, findViewById(R.id.container));

//        GlobalVar.getInstance().getNearbyPoi();

        poiObject = (PoiVO) Objects.requireNonNull(getIntent().getExtras()).getSerializable("poiObject");

        Boolean fromDiscover = getIntent().getExtras().getBoolean(Constants.FROM_DISCOVER);

        if (poiObject == null && GlobalVar.getInstance().getNearbyPoi() != null) {
            poiObject = GlobalVar.getInstance().getPoiMainCol().stream()
                    .filter(poiVO -> poiVO.getPoiId().equals(GlobalVar.getInstance().getNearbyPoi()))
                    .findFirst()
                    .orElse(null);
        }

        if (poiObject == null){
            loadLanguage();
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
            goToMain();
        }else {
            frameLayout = (FrameLayout) findViewById(R.id.poi_fragmet_container);
            bottomNavigationView = (BottomNavigationView) findViewById(R.id.poi_bottom_nav);

            // Creo bundle para enviar a fragment
            Bundle bundle = new Bundle();
            bundle.putSerializable(SELECTED_POI, poiObject);
            bundle.putBoolean(Constants.FROM_DISCOVER, fromDiscover);
            if(GlobalVar.getInstance().getNearbyPoi() != null){
                fromDiscover = Boolean.TRUE;
            }
            bundle.putBoolean("fromDiscover", fromDiscover);

            poiInfoFragment = new PoiInfoFragment();
            poiInfoFragment.setArguments(bundle);

            poiMapFragment = new PoiMapFragment();
            poiMapFragment.setArguments(bundle);

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.poi_fragmet_container,
                        poiInfoFragment).commit();
            }

            bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
                switch (menuItem.getItemId()) {
                    case R.id.nav_poi_info:
                        setFragment(poiInfoFragment);
                        return true;
                    case R.id.nav_poi_map:
                        setFragment(poiMapFragment);
                        return true;
                    default:
                        return false;
                }
            });

            this.setActive(R.id.nav_points_of_interest);

            drawData();

//        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

            // Get the SupportMapFragment and request notification
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.poi_description_map);
//        // when the map is ready to be used.
//        mapFragment.getMapAsync(this);

//        drawData();

            headerText.setText(R.string.points_of_interest);
        }


    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.poi_fragmet_container, fragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void drawData() {
        // Set header
        ImageView imageView = this.findViewById(R.id.poi_description_imageView);

        File dir = this.getDir(Constants.I_POI_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = poiObject.getImageName() + "." + poiObject.getExt();
        File mypath = new File(dir, filename);
        imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        TextView title = this.findViewById(R.id.poi_description_titleTextView);
        title.setText(poiObject.getTitle());
        // Set description

    }


    public void loadLanguage() {
        //Load language
        String langPref = "Language";
        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        if (language.equals("")) {
            if (Locale.getDefault().getLanguage().equals(Constants.ENGLISH_LANGUAGE) ||
                    Locale.getDefault().getLanguage().equals(Constants.SPANISH_LANGUAGE)) {
                language = Locale.getDefault().getLanguage();

            } else {
                language = Constants.ENGLISH_LANGUAGE;
            }
        }
        GlobalVar.getInstance().setCurrentLanguage(language);
        if (!language.isEmpty()) {
            //Change language
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            android.content.res.Configuration config = new android.content.res.Configuration();
            config.setLocale(locale);
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    private void goToMain() {
        Intent intent = new Intent(this, LoadDBActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
