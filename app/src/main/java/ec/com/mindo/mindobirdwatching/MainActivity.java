package ec.com.mindo.mindobirdwatching;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.bird.library.BirdLibraryMainActivity;
import ec.com.mindo.mindobirdwatching.bird.library.families.FamiliesMainActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.poi.PoiMainActivity;
import ec.com.mindo.mindobirdwatching.routes.RoutesMainActivity;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();


    private static CardView discoverHotspotCard;

    private static ImageView discoverImage;
    private static ImageView hotspotInCard;
    private static TextView discoverSubtitle;
    private static Context context;

    public static final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                addElement();
            }
            super.handleMessage(msg);
        }
    };

    public static void addElement() {
        if (GlobalVar.getInstance().getShowDiscoverMenu()) {
            String poiId = GlobalVar.getInstance().getNearbyPoi(); //main_scan_hotspot_btn
            PoiVO currentPoi = GlobalVar.getInstance().getPoiMainCol()
                    .stream()
                    .filter(poiVO -> poiVO.getPoiId().equals(poiId))
                    .findFirst()
                    .get();

            GlobalVar.getInstance().getPoiMainCol().remove(currentPoi);
            GlobalVar.getInstance().getPoiMainCol().add(0, currentPoi);

            File dir = context.getDir(Constants.I_POI_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
            String filename = currentPoi.getImageName() + "." + currentPoi.getExt();
            File mypath = new File(dir, filename);
            discoverImage.setImageDrawable(Drawable.createFromPath(mypath.toString()));
            discoverSubtitle.setText(currentPoi.getTitle());
            discoverHotspotCard.setVisibility(View.VISIBLE);
            hotspotInCard.setVisibility(View.VISIBLE);
        } else {
            GlobalVar.getInstance().setNearbyPoi(null);
            discoverHotspotCard.setVisibility(View.GONE);
            hotspotInCard.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();

        MainActivity.context = getApplicationContext();
        inflater.inflate(R.layout.activity_main, (ViewGroup) findViewById(R.id.container));

        if (GlobalVar.getInstance().getPoiMainCol() == null) {
            getAllPoiItems();
        }

        this.findViewById(R.id.main_poi_btn).setOnClickListener(v -> {
//            //loadingScreen.setVisibility(View.VISIBLE);
            startActivity(new Intent(this, PoiMainActivity.class));
        });

        this.findViewById(R.id.main_routes_btn).setOnClickListener(v -> {
//            //loadingScreen.setVisibility(View.VISIBLE);
            startActivity(new Intent(this, RoutesMainActivity.class));
        });

        // Bird library
        this.findViewById(R.id.main_bid_library_btn).setOnClickListener(v -> {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(this, FamiliesMainActivity.class);
                    String query = null;
                    intent.putExtra(Constants.QUERY_STRING, query);
                    startActivity(intent);
                }
        );

        // Discover hostpot
        this.findViewById(R.id.main_scan_hotspot_btn).setOnClickListener(v -> {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(this, BirdLibraryMainActivity.class);
                    intent.putExtra(Constants.FROM_DISCOVER, Boolean.TRUE);
                    String queryStr = null;
                    intent.putExtra(Constants.QUERY_STRING, queryStr);
                    startActivity(intent);
                }
        );
        discoverHotspotCard = this.findViewById(R.id.scan_hotspot_card);
        discoverImage = this.findViewById(R.id.main_scan_hotspot_btn);
        hotspotInCard = this.findViewById(R.id.hostpot_in_card);
        hotspotInCard.setVisibility(View.GONE);
        discoverSubtitle = this.findViewById(R.id.discover_subtitle);
        addElement();

        this.setActive(R.id.nav_main);
        headerText.setText(R.string.app_name);

        checkIfFirstTime();
    }

    private void checkIfFirstTime() {
        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        if (prefs.getBoolean(Constants.FIRST_TIME, true)) {
            // Si es la primera vez muestro el popup
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.notifications);
            builder.setMessage(getString(R.string.notif_dialog));
            // Si desea notificaciones
            builder.setPositiveButton(R.string.yes, (dialog, whichButton) -> updateFirstTime(prefs, true) );
            // No desea notificaciones
            builder.setNegativeButton(R.string.no, (dialog, whichButton) -> updateFirstTime(prefs, false) );
            AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);

            alertDialog.setOnShowListener(arg0 -> {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            });

            alertDialog.show();
        }
    }

    private void updateFirstTime(SharedPreferences prefs, boolean activateNotif){
        if(activateNotif){
            Util.activateNotifications(TAG, this, true);
        }
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.DOING_ROUTE, false);
        editor.putBoolean(Constants.SHOW_ALL_NOTIF, activateNotif);
        editor.putBoolean(Constants.FIRST_TIME, false);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

//        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
//                Activity.MODE_PRIVATE);
//        Boolean showAllNotif = prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false);
//        Boolean doingRoute = prefs.getBoolean(Constants.DOING_ROUTE, false);
//
//        if(showAllNotif || doingRoute) {
//            // Verifiacar si las notif estan activas
//            int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
//            if (response != ConnectionResult.SUCCESS) {
//                Log.d(TAG, "Google Play Services not available, show dialog to install");
//                GoogleApiAvailability.getInstance().getErrorDialog(this, response, 1).show();
//            } else {
//                Log.d(TAG, "Google Play Services is available");
//            }
//            LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
//            if (!GlobalVar.getInstance().getGeofenceMonitoring() && (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
//                    lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
//                Util.startGeofencingMonitoring(TAG, this);
//            }
//        }
        //loadingScreen.setVisibility(View.GONE);
        this.setActive(R.id.nav_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
//                Activity.MODE_PRIVATE);
//        Boolean showAllNotif = prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false);
//        Boolean doingRoute = prefs.getBoolean(Constants.DOING_ROUTE, false);
//
//        if(showAllNotif || doingRoute) {
//            // Verificar si las notif estan activas
//            if (GlobalVar.getInstance().getGoogleApiClient() != null) {
//                GlobalVar.getInstance().getGoogleApiClient().connect();
//            }
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    // Obtengo poi con sus details
    private void getAllPoiItems() {
        List<PoiVO> poiCol = new ArrayList<>();
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_POI, null);
        while (cursor.moveToNext()) {
            PoiVO poiVO = new PoiVO(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getDouble(3),
                    cursor.getDouble(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8)
            );
            poiCol.add(poiVO);
        }
        GlobalVar.getInstance().setPoiMainCol(poiCol);
        db.close();
        conn.close();
        //loadingScreen.setVisibility(View.GONE);
    }
}

