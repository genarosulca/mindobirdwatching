package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbFamilyVO implements Serializable {

    private String id;
    private String ext;
    private String imageName;
    private String name;
    private String scientificName;
    private Map<String, String> enInfo;
    private Map<String, String> esInfo;
    private String lastModifiedBy;
}
