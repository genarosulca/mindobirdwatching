package ec.com.mindo.mindobirdwatching.util;

import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

import ec.com.mindo.mindobirdwatching.entities.PoiVO;

public class GlobalVar {

    private static GlobalVar instance;


    private Boolean arSupported;
    private Boolean showDiscoverMenu = Boolean.FALSE;
    private List<PoiVO> poiMainCol;
    private String nearbyPoi;
    private Boolean geofenceMonitoring = false;
    private Bitmap screenshot;
    private Boolean geoFencesDefined = false;
    private GoogleApiClient googleApiClient;
    private String currentLanguage;
    private Integer permissionsGranted = 0;
    private Location currentLocation;
    private Boolean searchDone;
    private List<PoiVO> actualRoutePoiCol;

    public static synchronized GlobalVar getInstance() {
        if (instance == null) {
            instance = new GlobalVar();
        }
        return instance;
    }

    public Boolean getArSupported() {
        return arSupported;
    }

    public void setArSupported(Boolean arSupported) {
        this.arSupported = arSupported;
    }

    public Boolean getShowDiscoverMenu() {
        return showDiscoverMenu;
    }

    public void setShowDiscoverMenu(Boolean showDiscoverMenu) {
        this.showDiscoverMenu = showDiscoverMenu;
    }

    public List<PoiVO> getPoiMainCol() {
        return poiMainCol;
    }

    public void setPoiMainCol(List<PoiVO> poiMainCol) {
        this.poiMainCol = poiMainCol;
    }

    public String getNearbyPoi() {
        return nearbyPoi;
    }

    public void setNearbyPoi(String nearbyPoi) {
        this.nearbyPoi = nearbyPoi;
    }

    public Boolean getGeofenceMonitoring() {
        return geofenceMonitoring;
    }

    public void setGeofenceMonitoring(Boolean geofenceMonitoring) {
        this.geofenceMonitoring = geofenceMonitoring;
    }

    public Bitmap getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(Bitmap screenshot) {
        this.screenshot = screenshot;
    }

    public Boolean getGeoFencesDefined() {
        return geoFencesDefined;
    }

    public void setGeoFencesDefined(Boolean geoFencesDefined) {
        this.geoFencesDefined = geoFencesDefined;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    public String getCurrentLanguage() {
        return currentLanguage;
    }

    public void setCurrentLanguage(String currentLanguage) {
        this.currentLanguage = currentLanguage;
    }

    public Integer getPermissionsGranted() {
        return permissionsGranted;
    }

    public void setPermissionsGranted(Integer permissionsGranted) {
        this.permissionsGranted = permissionsGranted;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Boolean getSearchDone() {
        return searchDone;
    }

    public void setSearchDone(Boolean searchDone) {
        this.searchDone = searchDone;
    }

    public List<PoiVO> getActualRoutePoiCol() {
        return actualRoutePoiCol;
    }

    public void setActualRoutePoiCol(List<PoiVO> actualRoutePoiCol) {
        this.actualRoutePoiCol = actualRoutePoiCol;
    }
}
