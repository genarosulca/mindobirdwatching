package ec.com.mindo.mindobirdwatching.bird.library.description.images;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;

public class FullScreenImageActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ViewPager viewPager = findViewById(R.id.image_viewPager);
        FullScreenImageAdapter adapter = new FullScreenImageAdapter(this, (List<MediaResourceVO>) getIntent().getSerializableExtra("allItems"));
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getExtras().getInt("position"));
    }

}
