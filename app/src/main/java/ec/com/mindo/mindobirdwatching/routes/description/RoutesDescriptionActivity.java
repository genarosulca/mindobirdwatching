package ec.com.mindo.mindobirdwatching.routes.description;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;
import ec.com.mindo.mindobirdwatching.routes.description.info.RoutesInfoFragment;
import ec.com.mindo.mindobirdwatching.routes.description.map.RoutesMapFragment;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class RoutesDescriptionActivity extends BaseActivity {

    private static final String TAG = RoutesDescriptionActivity.class.getSimpleName();
    public static final String SELECTED_ROUTE = "selectedRoute";

    private RouteVO routeVO;
    private List<PoiVO> poiCol;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private CameraPosition mCameraPosition;
    private GoogleMap mMap;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private static final int ZOOM_PADDING = 200;

    private FrameLayout frameLayout;
    private BottomNavigationView bottomNavigationView;

    private RoutesInfoFragment routesInfoFragment;
    private RoutesMapFragment routesMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_poi_description, findViewById(R.id.container));

        boolean fromNotif = false;
        if(getIntent().getExtras() == null){
            routeVO = getRouteById();
            fromNotif = true;
        } else {
            routeVO = (RouteVO) getIntent().getExtras().getSerializable("selectedRoute");
        }

        this.setActive(R.id.nav_routes);

        frameLayout = (FrameLayout) findViewById(R.id.poi_fragmet_container);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.poi_bottom_nav);

        // Creo bundle para enviar a fragment
        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_ROUTE, routeVO);

        routesInfoFragment = new RoutesInfoFragment();
        routesInfoFragment.setArguments(bundle);

        routesMapFragment = new RoutesMapFragment();
        routesMapFragment.setArguments(bundle);

        if(savedInstanceState == null) {
            if(fromNotif){
                getSupportFragmentManager().beginTransaction().replace(R.id.poi_fragmet_container,
                        routesMapFragment).commit();
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.poi_fragmet_container,
                        routesInfoFragment).commit();
            }

        }

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.nav_poi_info:
                    setFragment(routesInfoFragment);
                    return true;
                case R.id.nav_poi_map:
                    setFragment(routesMapFragment);
                    return true;
                default:
                    return false;
            }
        });

        drawData();
        headerText.setText(R.string.routes);
    }

    public void drawData(){
        // Set header
        ImageView imageView = this.findViewById(R.id.poi_description_imageView);

        File dir = this.getDir(Constants.I_ROUTES_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = routeVO.getImageName() + "." + routeVO.getExt();
        File mypath = new File(dir,filename);
        imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));
        // Set header title
        TextView title = this.findViewById(R.id.poi_description_titleTextView);
        title.setText(routeVO.getName());

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.poi_fragmet_container, fragment)
                .commit();
    }

    public RouteVO getRouteById(){
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String queryString = prefs.getString(Constants.CURR_ROUTE_ID, "");

        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_ROUTE + " WHERE " +
                DBConstants.FIELD_ID + " = '" + queryString + "'", null);
        RouteVO routeVO = null;
        while (cursor.moveToNext()) {
            routeVO = new RouteVO(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8)
            );
        }
        return routeVO;
    }
//    @Override
//    public void onMapReady(GoogleMap map) {
//        mMap = map;
//        map.setOnMapLoadedCallback(() -> {
//            // Turn on the My Location layer and the related control on the map.
//            updateLocationUI();
//
////            // Pone la camara en una ubicacion inicial cercana, para evitar error de que mapa tiene size 0
////            moveCamera();
//
//            // put marker in map
//            putMarkers();
//        });
//        // Turn on the My Location layer and the related control on the map.
////        updateLocationUI();
////
//        // Pone la camara en una ubicacion inicial cercana, para evitar error de que mapa tiene size 0
//        moveCamera();
////
////        // put marker in map
////        putMarkers();
//    }
//
//    private void moveCamera(){
//        if (mMap == null) {
//            return;
//        }
//        PoiVO poiVO = poiCol.iterator().next();
//        LatLng poiLocation = new LatLng(poiVO.getLatitude(), poiVO.getLongitude());
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poiLocation, Constants.DEFAULT_ZOOM));
//    }
//
//    private void putMarkers(){
//        if (mMap == null) {
//            return;
//        }
//
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        // Add a markers and move the map's camera to the same location.
//
//        poiCol.forEach(poi -> {
//            LatLng latLng = new LatLng(poi.getLatitude(), poi.getLongitude());
//            mMap.addMarker(
//                    new MarkerOptions()
//                            .position(latLng)
//                            .title(poi.getTitle())
//            );
//            builder.include(latLng);
//        });
//
//        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), ZOOM_PADDING));
//    }
//
//    private void updateLocationUI() {
//        if (mMap == null) {
//            return;
//        }
//        try {
//            mMap.setMyLocationEnabled(true);
//            mMap.getUiSettings().setMyLocationButtonEnabled(true);
//        } catch (SecurityException e)  {
//            Log.e("Exception: %s", e.getMessage());
//        }
//    }



}
