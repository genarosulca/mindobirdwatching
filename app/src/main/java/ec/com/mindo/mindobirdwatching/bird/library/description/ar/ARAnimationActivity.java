package ec.com.mindo.mindobirdwatching.bird.library.description.ar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.PixelCopy;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.SkeletonNode;
import com.google.ar.sceneform.animation.ModelAnimator;
import com.google.ar.sceneform.rendering.AnimationData;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;

public class ARAnimationActivity extends AppCompatActivity {

    private static final String TAG = "AnimationSample";
    private static final int ANDY_RENDERABLE = 1;
    private ArFragment arFragment;
    // Model loader class to avoid leaking the activity context.
    private ModelLoader modelLoader;
    private ModelRenderable andyRenderable;
    private AnchorNode anchorNode;
    private SkeletonNode andy;
    // Controls animation playback.
    private ModelAnimator animator;
    // Index of the current animation playing.
    private int nextAnimation;
    // The UI to play next animation.
    private ImageView animationButton;
    private ImageView screenshotButton;

    private ImageView shareButton;
    private ImageView saveButton;
    private ImageView closeButton;
    private ImageView helpButton;

    //Screenshot
    private Bitmap screenshotBitmap;
    private Handler mHandler;
    private ImageView screenshotImage;
    private FrameLayout screenshotLayout;

    private Map<String, Integer> especiesConAr;
    private Boolean tieneAnimaciones;

    private FrameLayout helpBackground;
    private ScrollView helpContent;
    private Button closeHelpBtn;

    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        agregarEspeciesConAr();
        setContentView(R.layout.activity_ar_animation);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.sceneform_fragment);

        modelLoader = new ModelLoader(this);

        BirdVO selectedBird = (BirdVO) getIntent().getExtras().getSerializable("selectedBird");

//        Integer res = especiesConAr.get(selectedBird.getScientificName());

        modelLoader.loadModel(ANDY_RENDERABLE, especiesConAr.get(selectedBird.getScientificName()));

        // When a plane is tapped, the model is placed on an Anchor node anchored to the plane.
        arFragment.setOnTapArPlaneListener(this::onPlaneTap);

        helpButton = findViewById(R.id.ar_show_help);
        helpBackground = findViewById(R.id.ar_help_background);
        helpBackground.setVisibility(View.GONE);
        helpContent = findViewById(R.id.ar_help_scroll_view);
        helpContent.setVisibility(View.GONE);
        closeHelpBtn = findViewById(R.id.close_help_btn);

        helpButton.setOnClickListener(view -> {
            helpBackground.setVisibility(View.VISIBLE);
            helpContent.setVisibility(View.VISIBLE);
            helpButton.setVisibility(View.GONE);

        });

        closeHelpBtn.setOnClickListener(view -> {
            helpBackground.setVisibility(View.GONE);
            helpContent.setVisibility(View.GONE);
            helpButton.setVisibility(View.VISIBLE);
        });

        // Add a frame update listener to the scene to control the state of the buttons.
//        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onFrameUpdate);

        // Once the model is placed on a plane, this button plays the animations.
        animationButton = findViewById(R.id.animate);
        animationButton.setOnClickListener(this::onPlayAnimation);
        animationButton.setVisibility(View.GONE);

        // Tomar screenshot
        screenshotButton = findViewById(R.id.screenshot);
        screenshotButton.setOnClickListener(this::onTakeScreenshot);
        screenshotButton.setVisibility(View.GONE);

        mHandler = new Handler();

        screenshotLayout = findViewById(R.id.anim_screenshot_layout);
        screenshotLayout.setVisibility(View.GONE);
        screenshotImage = findViewById(R.id.anim_screenshot_image);

        shareButton = findViewById(R.id.screenshot_share);
        shareButton.setOnClickListener(view -> shareImage());
        shareButton.setVisibility(View.GONE);

        saveButton = findViewById(R.id.screenshot_save);
        saveButton.setOnClickListener(view -> {
            saveBitmapImage();
            hideScreenshotLayout();
        });
        saveButton.setVisibility(View.GONE);

        closeButton = findViewById(R.id.screenshot_close);
        closeButton.setOnClickListener(view -> hideScreenshotLayout());
        closeButton.setVisibility(View.GONE);

    }

    private void hideScreenshotLayout() {
        if (tieneAnimaciones) {
            animationButton.setVisibility(View.VISIBLE);
        }
        screenshotButton.setVisibility(View.VISIBLE);
        shareButton.setVisibility(View.GONE);
        saveButton.setVisibility(View.GONE);
        closeButton.setVisibility(View.GONE);
        screenshotLayout.setVisibility(View.GONE);
    }

    private void onPlayAnimation(View unusedView) {
        onPlayAnimation();
    }

    private void onPlayAnimation() {
        if (andyRenderable.getAnimationDataCount() > 0) {
            if (animator == null || !animator.isRunning()) {
                AnimationData data = andyRenderable.getAnimationData(nextAnimation);
                nextAnimation = (nextAnimation + 1) % andyRenderable.getAnimationDataCount();
                animator = new ModelAnimator(data, andyRenderable);
                animator.start();
            }
        }

    }

    /*
     * Used as the listener for setOnTapArPlaneListener.
     */
    private void onPlaneTap(HitResult hitResult, Plane unusedPlane, MotionEvent unusedMotionEvent) {
        if (andyRenderable == null) {
            return;
        }
        tieneAnimaciones = andyRenderable.getAnimationDataCount() > 0;
        // Create the Anchor.
        Anchor anchor = hitResult.createAnchor();
        if (anchorNode == null) {
            anchorNode = new AnchorNode(anchor);
            anchorNode.setParent(arFragment.getArSceneView().getScene());
            andy = new SkeletonNode();
            andy.setParent(anchorNode);
            andy.setRenderable(andyRenderable);
            // Muestro o escondo botones
//            if (animationButton.isOrWillBeHidden() && screenshotButton.isOrWillBeHidden()) {
                if (tieneAnimaciones) {
                    animationButton.setVisibility(View.VISIBLE);
                }
                screenshotButton.setVisibility(View.VISIBLE);
//            }
        }
        onPlayAnimation();
    }

    private void onTakeScreenshot(View unusedView) {
        getScreenshot();
    }

    private void getScreenshot() {
        screenshotButton.setVisibility(View.GONE);
        animationButton.setVisibility(View.GONE);
        shareButton.setVisibility(View.VISIBLE);
        saveButton.setVisibility(View.VISIBLE);
        closeButton.setVisibility(View.VISIBLE);
        final String filename = "NOMBRE";
        ArSceneView view = arFragment.getArSceneView();

        // Create a bitmap the size of the scene view.
        screenshotBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Bitmap.Config.ARGB_8888);

        // Create a handler thread to offload the processing of the image.
        final HandlerThread handlerThread = new HandlerThread("PixelCopier");
        handlerThread.start();
        // Make the request to copy.
        PixelCopy.request(view, screenshotBitmap, (copyResult) -> {
            if (copyResult == PixelCopy.SUCCESS) {
                showBitmap();
            } else {
                Toast toast = Toast.makeText(ARAnimationActivity.this,
                        "Failed to open preview: ", Toast.LENGTH_LONG);
                toast.show();
            }
            handlerThread.quitSafely();
        }, mHandler);
    }

    private void showBitmap() {
        // Imagen en bitmap
        screenshotImage.setImageBitmap(screenshotBitmap);
        screenshotLayout.setVisibility(View.VISIBLE);
    }

    void setRenderable(int id, ModelRenderable renderable) {
        if (id == ANDY_RENDERABLE) {
            this.andyRenderable = renderable;
        }
    }

    void onException(int id, Throwable throwable) {
        Toast toast = Toast.makeText(this, "Unable to load renderable: " + id, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.e(TAG, "Unable to load andy renderable", throwable);
    }

    private void shareImage() {
        shareImageUri(saveImage());
    }

    private Uri saveImage() {
        //TODO - Should be processed in another thread
        File imagesFolder = new File(getCacheDir(), "mindoBirdwatching");
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            screenshotBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(this, "ec.com.mindo.mindobirdwatching.fileprovider", file);
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));

        } catch (IOException e) {
            Log.d(TAG, "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }

    private void shareImageUri(Uri uri) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        startActivity(intent);
    }

    private void saveBitmapImage() {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/mindoBirdwatching");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        Log.i(TAG, "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            screenshotBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        Toast toast = Toast.makeText(this, getString(R.string.save_img_confirm), Toast.LENGTH_SHORT);

        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        if (screenshotLayout.getVisibility() == View.VISIBLE) {
            hideScreenshotLayout();
        } else {
            super.onBackPressed();
        }
    }

    private void agregarEspeciesConAr() {
        especiesConAr = new HashMap<>();
        especiesConAr.put("Haplophaedia lugens", R.raw.humming_bird_animated);
        especiesConAr.put("Ramphastos swainsonii", R.raw.tocotoucan);
    }
}
