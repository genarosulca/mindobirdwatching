package ec.com.mindo.mindobirdwatching.routes.description.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.internal.PolylineEncoding;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.tooltip.Tooltip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;
import ec.com.mindo.mindobirdwatching.routes.description.RoutesDescriptionActivity;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.MapItem;
import ec.com.mindo.mindobirdwatching.util.MyClusterManagerRenderer;
import ec.com.mindo.mindobirdwatching.util.Util;

public class RoutesMapFragment extends Fragment implements OnMapReadyCallback {

    private static String TAG = RoutesMapFragment.class.getSimpleName();

    private static final int ZOOM_PADDING = 200;

    private RouteVO routeVO;
    private List<PoiVO> poiCol;
    private List<PoiVO> orderedPois;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private GoogleMap mMap;

    private View inflatedObj;
    private MapView mapView;
    private GeoApiContext geoApiContext;
    private ClusterManager<MapItem> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<MapItem> mClusterMarkers = new ArrayList<>();
    //private Button startNavBtn;
    private FloatingActionButton startNavBtn;
    private Location lastLocation;
    private NotificationManagerCompat notificationManager;

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflatedObj = inflater.inflate(R.layout.fragment_route_map, container, false);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(inflatedObj.getContext());

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        notificationManager = NotificationManagerCompat.from(getActivity());
        try {
            mFusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            lastLocation = location;
                            orderPois(0);
                        }
                    });
        }catch (Exception e){
        }


        routeVO = (RouteVO) getArguments().getSerializable(RoutesDescriptionActivity.SELECTED_ROUTE);
        // Filtrar pois
        List<String> poiIds = new ArrayList<>(Arrays.asList(routeVO.getRelatedPois().split(",")));

        poiCol = GlobalVar.getInstance().getPoiMainCol().stream()
                .filter(poiVO -> poiIds.stream().anyMatch(id -> id.equals(poiVO.getPoiId())))
                .collect(Collectors.toList());
        StringBuilder orden = new StringBuilder();
        poiCol.forEach(poiVO -> orden.append(poiVO.getTitle() + "; "));
        Log.i(TAG, "Orden antes: " + orden.toString());

        StringBuilder ordenNuevo = new StringBuilder();
        poiCol.forEach(poiVO -> ordenNuevo.append(poiVO.getTitle() + "; "));
        Log.i(TAG, "Orden despues: " + ordenNuevo.toString());

        startNavBtn = inflatedObj.findViewById(R.id.start_route);

        SharedPreferences prefs = getActivity().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);

        changeButton(prefs.getBoolean(Constants.DOING_ROUTE, false));

        startNavBtn.setOnLongClickListener(view -> {
            showTooltip(view);
            Log.i(TAG, "Bien");
            return  true;

        });
        startNavBtn.setOnClickListener(view -> {
            SharedPreferences.Editor editor = prefs.edit();
            if(prefs.getBoolean(Constants.DOING_ROUTE, false)){
                // Stop doing route
                Util.deactivateNotifications(TAG, getActivity());
                if(prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false)){
                    Util.activateNotifications(TAG, getActivity(), true);
                }
                editor.putBoolean(Constants.DOING_ROUTE, false);
                changeButton(false);
                removeNotification();
                editor.apply();
            } else {
                // Start route
                if(prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false)){
                    Util.deactivateNotifications(TAG, getActivity());
                }
                startRoute(prefs);
            }

        });

        if(prefs.getBoolean(Constants.DOING_ROUTE, true)){
            createNotification(prefs);
        }


        return inflatedObj;
    }

    public void changeButton(boolean doingRoute){
        if(doingRoute){
            startNavBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.routeFlBtnStopColor)));
            startNavBtn.setImageResource(R.drawable.ic_stop);
        } else {
            startNavBtn.setImageResource(R.drawable.ic_directions_walk_white);
            startNavBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.routeFlBtnStartColor)));
        }
    }

    public void startRoute(SharedPreferences prefs){
        SharedPreferences.Editor editor = prefs.edit();
        GlobalVar.getInstance().setActualRoutePoiCol(poiCol);
        Util.activateNotifications(TAG, getActivity(), false);
        editor.putBoolean(Constants.DOING_ROUTE, true);
        editor.apply();

        StringBuilder url = new StringBuilder("https://www.google.com/maps/dir/?api=1");
        url.append("&travelmode=walking");
        url.append("&origin=").append(lastLocation.getLatitude()).append(",")
                .append(lastLocation.getLongitude());
        url.append("&destination=").append(poiCol.get(poiCol.size() - 1).getLatitude())
                .append(",").append(poiCol.get(poiCol.size() - 1).getLongitude());
        if(poiCol.size() > 1){
            url.append("&waypoints=");
            for(int i = 0; i < poiCol.size()-1; i++){
                url.append(poiCol.get(i).getLatitude()).append(",")
                        .append(poiCol.get(i).getLongitude()).append("%7C");
            }
        }
        int index = url.lastIndexOf("%7C");
        String urlStr = url.toString();
        if(index != -1) {
            urlStr = urlStr.substring(0,index);
        }
        Log.i(TAG, "urlStr" + urlStr);

        changeButton(true);
        createNotification(prefs);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(urlStr));
        startActivity(intent);
    }

    public void createNotification(SharedPreferences prefs){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.CURR_ROUTE_ID, routeVO.getRouteId());
        editor.apply();

        Intent notifIntent = new Intent(getActivity(), RoutesDescriptionActivity.class);
        notifIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, notifIntent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), Constants.CHANNEL_ID)
                .setSmallIcon(R.drawable.iconofondombw)
                .setContentTitle(getString(R.string.doing_route))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setContentText(getString(R.string.tap_to_open_route))
                .setOngoing(true);
        notificationManager.notify(Constants.NOTIF_ID_2, builder.build());
    }

    public void removeNotification(){
        notificationManager.cancel(Constants.NOTIF_ID_2);
    }

    public void showTooltip(View v){
        SharedPreferences prefs = getActivity().getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        boolean doingRoute = prefs.getBoolean(Constants.DOING_ROUTE, false);
        String tooltipText = doingRoute ? getString(R.string.stop) : getString(R.string.start);
        try {
            Tooltip tooltip = new Tooltip.Builder(v)
                    .setText(tooltipText)
                    .setTextColor(Color.WHITE)
                    .setGravity(GravityCompat.START)
                    .setCornerRadius(8f)
                    .setDismissOnClick(true)
                    .show();
            Handler handler = new Handler();
            handler.postDelayed(tooltip::dismiss, 1500);
        } catch (Exception e) {
            Log.i(TAG, "CAE");
        }
    }

    public void orderPois(int iteracion){
        if(iteracion == 0){
            // mas cercano a current location

            for(int i = iteracion; i < poiCol.size(); i++){
                poiCol.get(i).setDistance(getDistance(lastLocation.getLatitude(),
                        lastLocation.getLongitude(),
                        poiCol.get(i).getLatitude(), poiCol.get(i).getLongitude()));
            }
            poiCol.sort((poiVO, t1) -> Float.compare(poiVO.getDistance(), t1.getDistance()));
            orderPois(iteracion + 1);
        } else if(iteracion < poiCol.size()){
            poiCol.get(iteracion - 1).setDistance(iteracion - 1);
            for(int i = iteracion; i < poiCol.size(); i++){
                poiCol.get(i).setDistance(getDistance(poiCol.get(iteracion - 1).getLatitude(),
                        poiCol.get(iteracion - 1).getLongitude(),
                        poiCol.get(i).getLatitude(), poiCol.get(i).getLongitude()));
            }
            poiCol.sort((poiVO, t1) -> Float.compare(poiVO.getDistance(), t1.getDistance()));
            orderPois(iteracion + 1);
        }
    }

    private float getDistance(double ori_lat, double ori_long, double dest_lat, double dest_long){
        float results[] = new float[10];
        Location.distanceBetween(ori_lat, ori_long,
                dest_lat, dest_long, results);
        return results[0];
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = inflatedObj.findViewById(R.id.poi_description_map);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
            if(geoApiContext == null){
                geoApiContext = new GeoApiContext.Builder()
                        .apiKey(getString(R.string.my_api_key))
                        .build();
            }
        }
    }

    private void calculateDirections(){
        Log.d(TAG, "calculateDirections: calculating directions.");

        if(poiCol.size() > 1) {
            DirectionsApiRequest directions = new DirectionsApiRequest(geoApiContext);
            directions.alternatives(false);
            // Set origin
            directions.origin(
                    new com.google.maps.model.LatLng(
                            poiCol.get(0).getLatitude(),
                            poiCol.get(0).getLongitude()
                    )
            );
            // Set destination
            com.google.maps.model.LatLng destination = new com.google.maps.model.LatLng(
                    poiCol.get(poiCol.size() - 1).getLatitude(),
                    poiCol.get(poiCol.size() - 1).getLongitude()
            );
            // Set waypoints
            if(poiCol.size() > 2){
                List<com.google.maps.model.LatLng> waypoints = new ArrayList<>();
                for (int i = 1; i < poiCol.size() - 1; i++) {
                    waypoints.add(new com.google.maps.model.LatLng(
                            poiCol.get(i).getLatitude(),
                            poiCol.get(i).getLongitude()
                    ));
                }
                com.google.maps.model.LatLng[] array = waypoints.toArray(new com.google.maps.model.LatLng[waypoints.size()]);
                directions.waypoints(array);
            }
            directions.destination(destination).setCallback(new PendingResult.Callback<DirectionsResult>() {
                @Override
                public void onResult(DirectionsResult result) {
                    Log.d(TAG, "onResult: routes: " + result.routes[0].toString());
                    Log.d(TAG, "onResult: duration: " + result.routes[0].legs[0].duration);
                    Log.d(TAG, "onResult: distance: " + result.routes[0].legs[0].distance);
                    Log.d(TAG, "onResult: geocodedWayPoints: " + result.geocodedWaypoints[0].toString());
                    addPolylinesToMap(result);
                }

                @Override
                public void onFailure(Throwable e) {
                    Log.e(TAG, "onFailure: " + e.getMessage());

                }
            });
        }
    }

    private void addPolylinesToMap(final DirectionsResult result){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "run: result routes: " + result.routes.length);

                for(DirectionsRoute route: result.routes){
                    Log.d(TAG, "run: leg: " + route.legs[0].toString());
                    List<com.google.maps.model.LatLng> decodedPath = PolylineEncoding.decode(route.overviewPolyline.getEncodedPath());

                    List<LatLng> newDecodedPath = new ArrayList<>();

                    // This loops through all the LatLng coordinates of ONE polyline.
                    for(com.google.maps.model.LatLng latLng: decodedPath){

//                        Log.d(TAG, "run: latlng: " + latLng.toString());

                        newDecodedPath.add(new LatLng(
                                latLng.lat,
                                latLng.lng
                        ));
                    }
                    Polyline polyline = mMap.addPolyline(new PolylineOptions().addAll(newDecodedPath));
                    polyline.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    polyline.setClickable(true);

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Handler handler = new Handler();
        handler.postDelayed(() -> showTooltip(startNavBtn), 1000 );

    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        map.setOnMapLoadedCallback(() -> {
            // Turn on the My Location layer and the related control on the map.
            updateLocationUI();
            // put marker in map
            putMarkers();
            calculateDirections();
        });
        // Pone la camara en una ubicacion inicial cercana, para evitar error de que mapa tiene size 0
        moveCamera();
    }

    private void moveCamera(){
        if (mMap == null) {
            return;
        }
        PoiVO poiVO = poiCol.iterator().next();
        LatLng poiLocation = new LatLng(poiVO.getLatitude(), poiVO.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(poiLocation, Constants.DEFAULT_ZOOM));
    }

    private void putMarkers(){
        if (mMap == null) {
            return;
        }
        // MARCADORES NORMALES
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        // Add a markers and move the map's camera to the same location.
        poiCol.forEach(poi -> {
            LatLng latLng = new LatLng(poi.getLatitude(), poi.getLongitude());
            mMap.addMarker(
                    new MarkerOptions()
                            .position(latLng)
                            .title(poi.getTitle())
            );
            builder.include(latLng);
        });
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), ZOOM_PADDING));
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

}
