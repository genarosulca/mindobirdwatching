package ec.com.mindo.mindobirdwatching;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ec.com.mindo.mindobirdwatching.bird.library.BirdLibraryMainActivity;
import ec.com.mindo.mindobirdwatching.bird.library.families.FamiliesMainActivity;
import ec.com.mindo.mindobirdwatching.config.ConfigurationActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.FamilyVO;
import ec.com.mindo.mindobirdwatching.language.LanguageChoseActivity;
import ec.com.mindo.mindobirdwatching.poi.PoiMainActivity;
import ec.com.mindo.mindobirdwatching.routes.RoutesMainActivity;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    public TextView headerText;
    public SearchView searchView;
    public RelativeLayout searchFilter;
    public Boolean searchOpenned = Boolean.FALSE;
//    public RelativeLayout loadingScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        headerText = findViewById(R.id.header_text);

        Toolbar toolbar = findViewById(R.id.toolbar);

        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        loadingScreen = findViewById(R.id.base_loading_screen);

        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));

        RadioButton radioFamily = this.findViewById(R.id.radio_familiy);
        RadioButton radioBird = this.findViewById(R.id.radio_bird);
        searchFilter = this.findViewById(R.id.search_filter);

        searchView = findViewById(R.id.bird_search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(radioFamily.isChecked()){
                    if(getFilteredFamiliesCount(s.toLowerCase()) > 0) {
                        Intent intent = new Intent(getBaseContext(), FamiliesMainActivity.class);
                        intent.putExtra(Constants.QUERY_STRING, s.toLowerCase());
                        intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        closeSearch();
                    } else {
                        Toast toast = Toast.makeText(getBaseContext(),
                                getString(R.string.no_birds_found),
                                Toast.LENGTH_LONG);
                        toast.show();
                    }
                } else if(radioBird.isChecked()){
                    if(getFilteredBirdsCount(s.toLowerCase()) > 0){
                        Intent intent = new Intent(getBaseContext(), BirdLibraryMainActivity.class);
                        intent.putExtra(Constants.QUERY_STRING, s.toLowerCase());
                        intent.putExtra(Constants.FROM_DISCOVER, Boolean.FALSE);
                        intent.putExtra(Constants.FAMILY_ID, 0);
                        intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        closeSearch();
                    } else {
                        Toast toast = Toast.makeText(getBaseContext(),
                                getString(R.string.families_not_found),
                                Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchView.setOnSearchClickListener(view -> {
            headerText.setVisibility(View.GONE);
            searchFilter.setVisibility(View.VISIBLE);
            searchOpenned = Boolean.TRUE;
        });
        searchView.setOnCloseListener(() -> {
            headerText.setVisibility(View.VISIBLE);
            searchFilter.setVisibility(View.GONE);
            searchOpenned = Boolean.FALSE;
            return false;
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                if (!this.getComponentName().getClassName().equals(MainActivity.class.getName())) {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.nav_points_of_interest:
                if (!this.getComponentName().getClassName().equals(PoiMainActivity.class.getName())) {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    startActivity(new Intent(this, PoiMainActivity.class));
                    if (!this.getComponentName().getClassName().equals(MainActivity.class.getName())) {
                        finish();
                    }
                }
                break;
            case R.id.nav_bird_library:
                if (!this.getComponentName().getClassName().equals(FamiliesMainActivity.class.getName())) {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(this, FamiliesMainActivity.class);
                    String query = null;
                    intent.putExtra(Constants.QUERY_STRING, query);
                    startActivity(intent);
                    if (!this.getComponentName().getClassName().equals(MainActivity.class.getName())) {
                        finish();
                    }
                }
                break;
            case R.id.nav_routes:
                if (!this.getComponentName().getClassName().equals(RoutesMainActivity.class.getName())) {
//                    //loadingScreen.setVisibility(View.VISIBLE);
                    startActivity(new Intent(this, RoutesMainActivity.class));
                    if (!this.getComponentName().getClassName().equals(MainActivity.class.getName())) {
                        finish();
                    }
                }
                break;
            case R.id.nav_config:
                startActivity(new Intent(this, ConfigurationActivity.class));
                break;
            case R.id.nav_about:
                showAboutDialog();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(searchOpenned) {
            closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    private void closeSearch(){
        searchView.onActionViewCollapsed();
        headerText.setVisibility(View.VISIBLE);
        searchFilter.setVisibility(View.GONE);
        searchOpenned = Boolean.FALSE;
    }

    public void setActive(int id) {
        navigationView.setCheckedItem(id);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.menu_about)
                .setMessage(R.string.about_message)
                .setPositiveButton(getString(R.string.close), (dialog, whichButton) -> {});

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnShowListener(arg0 ->
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        );

        alertDialog.show();
    }

    private int getFilteredFamiliesCount(String queryStr){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_FAMILY + " WHERE LOWER(" +
                DBConstants.FIELD_NAME + ") LIKE '%" + queryStr + "%'", null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }

    private int getFilteredBirdsCount(String queryStr){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_BIRD + " WHERE LOWER(" +
                DBConstants.FIELD_NAME + ") LIKE '%" + queryStr + "%'", null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        return count;
    }
}
