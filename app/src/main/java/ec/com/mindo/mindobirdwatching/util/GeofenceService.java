package ec.com.mindo.mindobirdwatching.util;

import android.app.Activity;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;
import java.util.Locale;

import ec.com.mindo.mindobirdwatching.MainActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;

public class GeofenceService extends IntentService {

    public static final String TAG = "GeofenceService";

    public GeofenceService(){
        super(TAG);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        loadLanguage();

        Intent notifIntent = new Intent(this, PoiDescriptionActivity.class);
        notifIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notifIntent.putExtra(Constants.FROM_DISCOVER, Boolean.TRUE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notifIntent, 0);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.CHANNEL_ID)
                .setSmallIcon(R.drawable.iconofondombw)
                .setContentTitle(getResources().getString(R.string.entered_fence))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if(geofencingEvent.hasError()){
            Log.e(TAG, GeofenceStatusCodes.getStatusCodeString(geofencingEvent.getErrorCode()));
        } else {
            int transition = geofencingEvent.getGeofenceTransition();
            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
            Geofence geofence = geofences.get(0);
            String requestId = geofence.getRequestId();

            if(transition == Geofence.GEOFENCE_TRANSITION_ENTER){
                // Add notification
                builder.setContentText(getResources().getString(R.string.tap_to_open));
                notificationManager.notify(Constants.NOTIF_ID, builder.build());
                GlobalVar.getInstance().setNearbyPoi(requestId);
                GlobalVar.getInstance().setShowDiscoverMenu(true);
                // Put message in handler
                Message msg = new Message();
                msg.what = 1;
                MainActivity.mHandler.sendMessage(msg);
                Log.d(TAG, "Entering geofence " + requestId);
            } else {
                builder.setContentText("Exiting geofence " + requestId);
                GlobalVar.getInstance().setNearbyPoi(null);
                GlobalVar.getInstance().setShowDiscoverMenu(false);
                // Remove notification
                notificationManager.cancel(Constants.NOTIF_ID);
                Message msg = new Message();
                msg.what = 1;
                MainActivity.mHandler.sendMessage(msg);
                Log.d(TAG, "Exiting geofence " + requestId);
            }
        }
    }

    public void loadLanguage() {
        //Load language
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        if(language.equals("")){
            if(Locale.getDefault().getLanguage().equals(Constants.ENGLISH_LANGUAGE) ||
                    Locale.getDefault().getLanguage().equals(Constants.SPANISH_LANGUAGE)){
                language = Locale.getDefault().getLanguage();

            } else {
                language = Constants.ENGLISH_LANGUAGE;
            }
        }
        GlobalVar.getInstance().setCurrentLanguage(language);
        if (!language.isEmpty()) {
            //Change language
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            android.content.res.Configuration config = new android.content.res.Configuration();
            config.setLocale(locale);
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }


}
