package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BirdVO implements Serializable {

    private String birdId;
    private String familyId;
    private String imageName;
    private String ext;
    private Boolean inDanger;
    private Boolean activateAr;
    private String name;
    private String scientificName;
    private String lifetime;
    private String speed;
    private String weight;
    private String size;
    private String info;
    private String dangerInfo;

    private String familyScientificName;

    private List<MediaResourceVO> imageGallery;
    private List<MediaResourceVO> videoGallery;
    private List<MediaResourceVO> soundsGallery;

    public BirdVO(String birdId, String familyId, String imageName, String ext, Boolean inDanger, String name,
                  String scientificName, String lifetime, String speed, String weight, String size,
                  String info, String dangerInfo, Boolean activateAr) {
        this.familyId = familyId;
        this.birdId = birdId;
        this.imageName = imageName;
        this.ext = ext;
        this.inDanger = inDanger;
        this.name = name;
        this.scientificName = scientificName;
        this.lifetime = lifetime;
        this.speed = speed;
        this.weight = weight;
        this.size = size;
        this.info = info;
        this.dangerInfo = dangerInfo;
        this.activateAr = activateAr;
    }

    public BirdVO(String birdId, String familyId, String imageName, String ext, Boolean inDanger, String name,
                  String scientificName, String lifetime, String speed, String weight, String size,
                  String info, String dangerInfo, List<MediaResourceVO> imageGallery, List<MediaResourceVO> videoGallery,
                  List<MediaResourceVO> soundsGallery, Boolean activateAr) {
        this.familyId = familyId;
        this.birdId = birdId;
        this.imageName = imageName;
        this.ext = ext;
        this.inDanger = inDanger;
        this.name = name;
        this.scientificName = scientificName;
        this.lifetime = lifetime;
        this.speed = speed;
        this.weight = weight;
        this.size = size;
        this.info = info;
        this.dangerInfo = dangerInfo;
        this.imageGallery = imageGallery;
        this.videoGallery = videoGallery;
        this.soundsGallery = soundsGallery;
        this.activateAr = activateAr;
    }


}
