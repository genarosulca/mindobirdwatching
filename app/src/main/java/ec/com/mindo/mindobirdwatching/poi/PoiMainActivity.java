package ec.com.mindo.mindobirdwatching.poi;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class PoiMainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_poi_main, findViewById(R.id.container));

        GridView gridview = this.findViewById(R.id.poi_gridview);

        PoiMainAdapter customAdapter = new PoiMainAdapter(this, GlobalVar.getInstance().getPoiMainCol());
        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            //loadingScreen.setVisibility(View.VISIBLE);
            Intent intent = new Intent(view.getContext(), PoiDescriptionActivity.class);
            intent.putExtra("poiObject", GlobalVar.getInstance().getPoiMainCol().get(position));
            startActivity(intent);
        });
        this.setActive(R.id.nav_points_of_interest);
        headerText.setText(R.string.points_of_interest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //loadingScreen.setVisibility(View.GONE);
    }
}
