package ec.com.mindo.mindobirdwatching.bird.library.families;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.FamilyVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class FamiliesAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<FamilyVO> listStorage;
    private Context context;

    public FamiliesAdapter(Context context, List<FamilyVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_families_main, parent, false);
            listViewHolder.imageInListView = (ImageView)convertView.findViewById(R.id.families_main_imageView);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.families_main_titleTextView);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getName());

        File dir = context.getDir(Constants.I_FAMILY_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getImageName() + "." + listStorage.get(position).getExt();
        File mypath = new File(dir,filename);
        listViewHolder.imageInListView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        listViewHolder.imageInListView.setImageMatrix(Util.scaleTop(listViewHolder.imageInListView, context));

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
        ImageView imageInListView;
    }
}
