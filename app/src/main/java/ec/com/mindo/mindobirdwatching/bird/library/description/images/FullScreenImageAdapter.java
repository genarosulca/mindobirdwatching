package ec.com.mindo.mindobirdwatching.bird.library.description.images;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class FullScreenImageAdapter extends PagerAdapter {
    private List<MediaResourceVO> images;
    private LayoutInflater inflater;
    private Context context;
    private Activity activity;


    public FullScreenImageAdapter(Activity activity, List<MediaResourceVO> images) {
        this.context = activity.getBaseContext();
        this.images = images;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.listview_slide_image, view, false);

        final PhotoView imageView = (PhotoView) imageLayout
                .findViewById(R.id.full_screen_image);

        imageView.setOnPhotoTapListener((v, x, y) -> {
            int flags = activity.getWindow().getDecorView().getSystemUiVisibility();
            if ((flags & View.SYSTEM_UI_FLAG_IMMERSIVE) != 0) {
                Log.i("Quito full screen", "Quito full screen");
                showSystemUI();
            } else {
                hideSystemUI();
            }
        });


//        Uri uri = Util.getUri(Constants.BIRDS_DIR,
//                images.get(position).getResourceName(),
//                images.get(position).getExt());

        File dir = context.getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = images.get(position).getResourceName() + "." + images.get(position).getExt();
        File mypath = new File(dir,filename);

        Glide.with(context)
                .load(mypath)//genaro
                .into(imageView);

        view.addView(imageLayout, 0);
        return imageLayout;
    }

    public static RequestOptions getOptions() {
        RequestOptions mBaseRequestOptions = null;
        if (mBaseRequestOptions == null) {
            mBaseRequestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .format(DecodeFormat.PREFER_ARGB_8888);
        }
        return mBaseRequestOptions.clone();
    }

    public static RequestOptions getSplashOptions() {
        RequestOptions mSplashRequestOptions = null;
        if (mSplashRequestOptions == null) {
            mSplashRequestOptions = getOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .onlyRetrieveFromCache(true);
        }
        return mSplashRequestOptions;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI() {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
}
