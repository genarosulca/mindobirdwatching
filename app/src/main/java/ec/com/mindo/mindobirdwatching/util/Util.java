package ec.com.mindo.mindobirdwatching.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;

import static android.content.Context.LOCATION_SERVICE;

public final class Util {

    private static FusedLocationProviderClient fusedLocationProviderClient;
    private static LocationCallback locationCallback;
    private static GeofencingClient geofencingClient;
    private static PendingIntent geofencePendingIntent;
    private static final Integer RADIUS = 25; // radio en metros 25, 250 para probar

    public static String loadJSONFromFile(String fileLocation) {
        String json;
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), fileLocation);
            FileInputStream stream = new FileInputStream(yourFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }

    public static Uri getUri(String dir, String resourceName, String ext){
        return Uri.parse(Environment.getExternalStorageDirectory()
                + "/" + dir.concat(resourceName).concat(".").concat(ext));
    }

    private static void hideArButton(Activity activity, BirdVO selectedBird, Boolean fromDiscover){
        BottomNavigationItemView menu = activity.findViewById(R.id.nav_ar);
        if(GlobalVar.getInstance().getArSupported()) {
            if (selectedBird.getActivateAr() && fromDiscover) {
                //Add AR button
                menu.setVisibility(View.VISIBLE);
            } else {
                //Hide AR button
                menu.setVisibility(View.GONE);
            }
        } else {
            menu.setVisibility(View.GONE);
        }
    }

    private static void hideGalleries(Activity activity, BirdVO selectedBird){
        BottomNavigationItemView imageGallery = activity.findViewById(R.id.nav_images);
        BottomNavigationItemView videoGallery = activity.findViewById(R.id.nav_videos);
        BottomNavigationItemView soundGallery = activity.findViewById(R.id.nav_sound);
        if(CollectionUtils.isEmpty(selectedBird.getImageGallery())){
            imageGallery.setVisibility(View.GONE);
        }
        if(CollectionUtils.isEmpty(selectedBird.getVideoGallery())){
            videoGallery.setVisibility(View.GONE);
        }
        if(CollectionUtils.isEmpty(selectedBird.getSoundsGallery())){
            soundGallery.setVisibility(View.GONE);
        }
    }

    public static void hideUnavaliableButtons(Activity activity, BirdVO selectedBird, Boolean fromDiscover){
        hideArButton(activity, selectedBird, fromDiscover);
        hideGalleries(activity, selectedBird);
    }

    public static String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    public static Matrix scaleTop(ImageView imageView, Context context){
        final Matrix matrix = imageView.getImageMatrix();
        final float imageWidth = imageView.getDrawable().getIntrinsicWidth();
        final int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        final float scaleRatio = screenWidth / imageWidth;
        matrix.setScale(scaleRatio, scaleRatio);
        return matrix;
    }

    public static void deactivateNotifications(String tag, Activity activity){
        if(fusedLocationProviderClient != null){
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        }
        stopGeofenceMonitoring(tag, activity);
    }

    public static void activateNotifications(String tag, Activity activity, Boolean allNotif){
        Log.i(tag, "Activando notificaciones");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        geofencingClient = LocationServices.getGeofencingClient(activity);

        LocationManager lm = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.location_not_active);
            builder.setMessage(R.string.location_na_message);
            builder.setPositiveButton(R.string.dialog_accept, (dialogInterface, i) -> {
                // Show location settings when the user acknowledges the alert dialog
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }


        if (!GlobalVar.getInstance().getGeofenceMonitoring()) {
            GlobalVar.getInstance().setGoogleApiClient(new GoogleApiClient.Builder(activity)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            Log.d(tag, "Connected to googleApiClient");
                            startLocationMonitoring(tag);
                            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                                    lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                                startGeofencingMonitoring(tag, activity, allNotif);
                            }
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Log.d(tag, "Suspended connection to googleApiClient");
                        }
                    })
                    .addOnConnectionFailedListener(connectionResult -> Log.d(tag,
                            "Failed to connect to GoogleApiclient - " + connectionResult.getErrorMessage()))
                    .build()
            );
        }
        if (GlobalVar.getInstance().getGoogleApiClient() != null) {
            GlobalVar.getInstance().getGoogleApiClient().connect();
        }

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    GlobalVar.getInstance().setCurrentLocation(location);
                    Log.d(tag, "Location updated lat/long: " + location.getLatitude() + " " + location.getLongitude());
                }
            }
        };
    }

    private static void startLocationMonitoring(String tag) {
        Log.d(tag, "start Location called");
        try {
            LocationRequest locationRequest = LocationRequest.create()
                    .setInterval(10000)
                    .setFastestInterval(5000)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // Que hacer cuando location cambie
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
        } catch (SecurityException e) {
            Log.d(tag, "Security exception - " + e.getMessage());
        } catch (Exception ex) {
            Log.d(tag, "Excetion en startLocationMonitoring " + ex.getMessage());
        }
    }

    public static void startGeofencingMonitoring(String tag, Activity activity, Boolean allNotif) {
        Log.d(tag, "Start monitoring called");
        try {
            List<Geofence> geofences = new ArrayList<>();

            if(allNotif){
                GlobalVar.getInstance().getPoiMainCol().forEach(poiMainObject -> {
                    Geofence geofence = new Geofence.Builder()
                            .setRequestId(poiMainObject.getPoiId())
                            // lat, long kruger -0.184974, -78.474604
                            .setCircularRegion(poiMainObject.getLatitude(), poiMainObject.getLongitude(), RADIUS)
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .setNotificationResponsiveness(1000)
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                            .build();
                    geofences.add(geofence);
                });
            } else {
                GlobalVar.getInstance().getActualRoutePoiCol().forEach(poiMainObject -> {
                    Geofence geofence = new Geofence.Builder()
                            .setRequestId(poiMainObject.getPoiId())
                            // lat, long kruger -0.184974, -78.474604
                            .setCircularRegion(poiMainObject.getLatitude(), poiMainObject.getLongitude(), RADIUS)
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .setNotificationResponsiveness(1000)
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                            .build();
                    geofences.add(geofence);
                });
            }


            if(CollectionUtils.isEmpty(geofences)){
                GlobalVar.getInstance().setGeofenceMonitoring(Boolean.FALSE);
            } else {
                GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                        .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                        .addGeofences(geofences)
                        .build();

                if (!GlobalVar.getInstance().getGoogleApiClient().isConnected()) {
                    Log.d(tag, "GoogleApiClient is not connected");
                } else {
                    geofencingClient.addGeofences(geofencingRequest, getGeofencePendingIntent(activity))
                            .addOnSuccessListener(activity, aVoid -> Log.d(tag, "Succesfully added fence"))
                            .addOnFailureListener(activity, e -> Log.d(tag, "Failed to add geofence " + e.getMessage()));
                }
                GlobalVar.getInstance().setGeofenceMonitoring(Boolean.TRUE);
            }
        } catch (SecurityException e) {
            Log.d(tag, "Security exception - " + e.getMessage());
        }
    }

    private static PendingIntent getGeofencePendingIntent(Activity activity) {
        // Reuse the PendingIntent if we already have it.
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent intent = new Intent(activity, GeofenceService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        geofencePendingIntent = PendingIntent.getService(activity, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    private static void stopGeofenceMonitoring(String tag, Activity activity) {
        Log.d(tag, "Stop monitoring called");
//        ArrayList<String> geofencesIds = new ArrayList<>();
//        if(allNotif){
//            GlobalVar.getInstance().getPoiMainCol().forEach(poiMainObject -> geofencesIds.add(poiMainObject.getPoiId()));
//        } else {
//            GlobalVar.getInstance().getActualRoutePoiCol().forEach(poiMainObject -> geofencesIds.add(poiMainObject.getPoiId()));
//        }

        if(geofencingClient != null){
            geofencingClient.removeGeofences(getGeofencePendingIntent(activity))
                    .addOnSuccessListener(activity, aVoid -> Log.d(tag, "Removed geofence "))
                    .addOnFailureListener(activity, e -> Log.d(tag, "Failed to remove geofence " + e.getMessage()));
        }
        GlobalVar.getInstance().setGeofenceMonitoring(Boolean.FALSE);
    }


}
