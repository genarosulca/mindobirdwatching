package ec.com.mindo.mindobirdwatching.routes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class RoutesMainAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<RouteVO> listStorage;
    private Context context;

    public RoutesMainAdapter(Context context, List<RouteVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_routes_main, parent, false);
            listViewHolder.imageInListView = (ImageView)convertView.findViewById(R.id.routes_main_imageView);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.routes_main_titleTextView);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getName());

        File dir = context.getDir(Constants.I_ROUTES_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getImageName() + "." + listStorage.get(position).getExt();
        File mypath = new File(dir,filename);
        listViewHolder.imageInListView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
        ImageView imageInListView;
    }
}
