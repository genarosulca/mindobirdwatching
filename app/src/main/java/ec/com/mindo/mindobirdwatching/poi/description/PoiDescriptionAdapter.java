package ec.com.mindo.mindobirdwatching.poi.description;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ec.com.mindo.mindobirdwatching.R;

@Deprecated
public class PoiDescriptionAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<String> listStorage;
    private Context context;

    public PoiDescriptionAdapter(Context context, List<String> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_poi_description, parent, false);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.textView);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position));

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
    }
}
