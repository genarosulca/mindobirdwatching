package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MediaResourceVO implements Serializable {

    private String birdId;
    private String resourceName;
    private String ext;
    private String thumnail;
    private String thumbnailExt;
    private String author;

    public MediaResourceVO(String birdId, String resourceName, String ext, String author) {
        this.birdId = birdId;
        this.resourceName = resourceName;
        this.ext = ext;
        this.author = author;
    }

    public MediaResourceVO(String birdId, String resourceName, String ext, String thumnail,
                           String thumbnailExt, String author) {
        this.birdId = birdId;
        this.resourceName = resourceName;
        this.ext = ext;
        this.thumnail = thumnail;
        this.thumbnailExt = thumbnailExt;
        this.author = author;
    }
}
