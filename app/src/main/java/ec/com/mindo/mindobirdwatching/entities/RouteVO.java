package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RouteVO implements Serializable {

    private String routeId;
    private String imageName;
    private String ext;
    private String name;
    private String relatedPois;
    private String relatedBirds;
    private String distance;
    private String distanceNotation;
    private String description;
    private List<PoiVO> poiCol;

    public RouteVO(String routeId, String imageName, String ext, String name, String relatedPois,
                   String relatedBirds, String distance, String distanceNotation, String description) {
        this.routeId = routeId;
        this.imageName = imageName;
        this.ext = ext;
        this.name = name;
        this.relatedPois = relatedPois;
        this.relatedBirds = relatedBirds;
        this.distance = distance;
        this.distanceNotation = distanceNotation;
        this.description = description;
    }
}
