package ec.com.mindo.mindobirdwatching.bird.library.families;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.BirdLibraryMainActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.FamilyVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class FamiliesMainActivity extends BaseActivity {

    private List<FamilyVO> familiesCol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_birdlibrary_main, (ViewGroup) findViewById(R.id.container));

        GridView gridview = (GridView) this.findViewById(R.id.birdlibrary_main_gridview);

        String queryString = getIntent().getExtras().getString(Constants.QUERY_STRING);

        FamiliesAdapter customAdapter;

        if(queryString == null){
            familiesCol = getAllFamilies();
        } else {
            familiesCol = getFilteredFamilies(queryString);
//            if(GlobalVar.getInstance().getSearchDone()){
//                // Si ya se habia hecho la busqueda elimino la penultima actividad de historia
//            } else {
//                GlobalVar.getInstance().setSearchDone(Boolean.FALSE);
//            }
        }

        headerText.setText(getString(R.string.families));
        customAdapter = new FamiliesAdapter(this, familiesCol);

        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            //loadingScreen.setVisibility(View.VISIBLE);
            Intent intent = new Intent(view.getContext(), BirdLibraryMainActivity.class);
            intent.putExtra(Constants.FROM_DISCOVER, Boolean.FALSE);
            intent.putExtra(Constants.FAMILY_ID, familiesCol.get(position).getFamilyId());
            String queryStr = null;
            intent.putExtra(Constants.QUERY_STRING, queryStr);
            startActivity(intent);
        });
        searchView.setVisibility(View.VISIBLE);
        this.setActive(R.id.nav_bird_library);
    }

    private List<FamilyVO> getAllFamilies(){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_FAMILY, null);
        return armarLista(cursor);
    }

    private List<FamilyVO> getFilteredFamilies(String queryStr){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_FAMILY + " WHERE LOWER(" +
                DBConstants.FIELD_NAME + ") LIKE '%" + queryStr + "%'", null);
        return armarLista(cursor);
    }

    private List<FamilyVO> armarLista(Cursor cursor){
        List<FamilyVO> families = new ArrayList<>();
        while (cursor.moveToNext()){
            families.add(
                    new FamilyVO(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4)
                    )
            );
        }
        return families;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
