package ec.com.mindo.mindobirdwatching.bird.library.description.ar;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.PixelCopy;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import ec.com.mindo.mindobirdwatching.R;

public class ARPlaneDetectionActivity extends AppCompatActivity {

    private static final String TAG = ARPlaneDetectionActivity.class.getSimpleName();
    private static final double MIN_OPENGL_VERSION = 3.0;
    private static final int CAMERA_REQUEST = 1888;

    private ArFragment planeDetectionFragment;
    private ModelRenderable birdRenderable;
    private ImageView takeScreenshot, screenshotImage, deleteScreenshot, shareScreenshot, saveScreenshot;
    private FrameLayout arUiButtons;
    private FrameLayout screenshotLayout;
    private RelativeLayout screenShotButtons;
    private Handler mHandler;

    private Boolean showArUi = Boolean.FALSE, showScreenshotUi = Boolean.FALSE;

    private Bitmap inputBMP = null, bmp, bmp1, screenshotBitmap;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }

        setContentView(R.layout.activity_plane_detection);
        planeDetectionFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.plane_detection_fragment);

        // When you build a Renderable, Sceneform loads its resources in the background while returning
        // a CompletableFuture. Call thenAccept(), handle(), or check isDone() before calling get().
        ModelRenderable.builder()
                .setSource(this, R.raw.tocotoucan)
                .build()
                .thenAccept(renderable -> birdRenderable = renderable)
                .exceptionally(
                        throwable -> {
                            Toast toast =
                                    Toast.makeText(this, "Unable to load bird renderable", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return null;
                        });

        planeDetectionFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    if (birdRenderable == null) {
                        return;
                    }

                    // Create the Anchor.
                    Anchor anchor = hitResult.createAnchor();
                    AnchorNode anchorNode = new AnchorNode(anchor);
                    anchorNode.setParent(planeDetectionFragment.getArSceneView().getScene());

                    // Create the transformable andy and add it to the anchor.
                    TransformableNode andy = new TransformableNode(planeDetectionFragment.getTransformationSystem());
                    andy.setParent(anchorNode);
                    andy.setRenderable(birdRenderable);
                    andy.select();
                    showArUi = Boolean.TRUE;
                    showScreenshotUi = Boolean.FALSE;
                    showArUiFunc();
                });

        setInitialVisibility();
        initLayoutButtons();
        mHandler=new Handler();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Returns false and displays an error message if Sceneform can not run, true if Sceneform can run
     * on this device. From Google
     *
     * <p>Sceneform requires Android N on the device as well as OpenGL 3.0 capabilities.
     *
     * <p>Finishes the activity if Sceneform can not run
     */
    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initLayoutButtons();
    }

    private void setInitialVisibility(){
        findViewById(R.id.ar_ui_screenshot_portrait_buttons).setVisibility(View.GONE);
        findViewById(R.id.ar_ui_portrait_buttons).setVisibility(View.GONE);
        findViewById(R.id.ar_ui_screenshot_landscape_buttons).setVisibility(View.GONE);
        findViewById(R.id.ar_ui_landscape_buttons).setVisibility(View.GONE);
    }

    public void showArUiFunc(){
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(showArUi){
                findViewById(R.id.ar_ui_screenshot_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_portrait_buttons).setVisibility(View.VISIBLE);
                findViewById(R.id.ar_ui_screenshot_landscape_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_landscape_buttons).setVisibility(View.GONE);
            } else if (showScreenshotUi){
                findViewById(R.id.ar_ui_screenshot_portrait_buttons).setVisibility(View.VISIBLE);
                findViewById(R.id.ar_ui_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_screenshot_landscape_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_landscape_buttons).setVisibility(View.GONE);
            }
        } else {
            if(showArUi){
                findViewById(R.id.ar_ui_screenshot_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_screenshot_landscape_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_landscape_buttons).setVisibility(View.VISIBLE);
            } else if (showScreenshotUi){
                findViewById(R.id.ar_ui_screenshot_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_portrait_buttons).setVisibility(View.GONE);
                findViewById(R.id.ar_ui_screenshot_landscape_buttons).setVisibility(View.VISIBLE);
                findViewById(R.id.ar_ui_landscape_buttons).setVisibility(View.GONE);
            }

        }
    }

    private void initLayoutButtons() {


        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // code for portrait mode
            arUiButtons = findViewById(R.id.ar_ui_portrait_buttons);
            // Take screenshot button
            takeScreenshot = findViewById(R.id.portrait_take_screenshot);
            // Screenshot
            // Buttons
            screenShotButtons = findViewById(R.id.ar_ui_screenshot_portrait_buttons);
            deleteScreenshot = findViewById(R.id.portrait_screenshot_delete);
            shareScreenshot = findViewById(R.id.portrait_screenshot_share);
            saveScreenshot = findViewById(R.id.portrait_screenshot_save);
        } else {
            // code for landscape mode
            arUiButtons = findViewById(R.id.ar_ui_landscape_buttons);
            // Take screenshot button
            takeScreenshot = findViewById(R.id.landscape_take_screenshot);
            screenShotButtons = findViewById(R.id.ar_ui_screenshot_landscape_buttons);
            deleteScreenshot = findViewById(R.id.landscape_screenshot_delete);
            shareScreenshot = findViewById(R.id.landscape_screenshot_share);
            saveScreenshot = findViewById(R.id.landscape_screenshot_save);
        }

        showArUiFunc();

        // Screenshot layout
        screenshotLayout = findViewById(R.id.screenshot_layout);
        if(showScreenshotUi) {
            screenshotLayout.setVisibility(View.VISIBLE);
        } else {
            screenshotLayout.setVisibility(View.GONE);
        }

        // Screenshot image
        screenshotImage = findViewById(R.id.screenshot_image);
        takeScreenshot.setOnClickListener(view -> {
            getScreenshot();
            showArUi = Boolean.FALSE;
            showScreenshotUi = Boolean.TRUE;
            showArUiFunc();
        });
        deleteScreenshot.setOnClickListener(view -> {
            closeScreenshotView();
        });
        shareScreenshot.setOnClickListener(view -> {
            screenshotLayout.setVisibility(View.GONE);
            arUiButtons.setVisibility(View.VISIBLE);
            shareImage();
            showScreenshotUi = Boolean.FALSE;
            showArUi = Boolean.TRUE;
            showArUiFunc();
        });
        saveScreenshot.setOnClickListener(view -> {
            screenshotLayout.setVisibility(View.GONE);
            arUiButtons.setVisibility(View.VISIBLE);
            saveBitmapImage();
            showToast(getString(R.string.image_saved));
            showScreenshotUi = Boolean.FALSE;
            showArUi = Boolean.TRUE;
            showArUiFunc();
        });
    }

    private void closeScreenshotView() {
        screenshotLayout.setVisibility(View.GONE);
        arUiButtons.setVisibility(View.VISIBLE);
        showScreenshotUi = Boolean.FALSE;
        showArUi = Boolean.TRUE;
        showArUiFunc();
    }

    private void showToast(String action){
        Toast toast = Toast.makeText(this,
                action, Toast.LENGTH_LONG);
        toast.show();
    }

    private void getScreenshot() {
        final String filename = "NOMBRE";
        ArSceneView view = planeDetectionFragment.getArSceneView();

        // Create a bitmap the size of the scene view.
        screenshotBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Bitmap.Config.ARGB_8888);

        // Create a handler thread to offload the processing of the image.
        final HandlerThread handlerThread = new HandlerThread("PixelCopier");
        handlerThread.start();
        // Make the request to copy.
        PixelCopy.request(view, screenshotBitmap, (copyResult) -> {
            if (copyResult == PixelCopy.SUCCESS) {
                showBitmap();
            } else {
                Toast toast = Toast.makeText(ARPlaneDetectionActivity.this,
                        "Failed to open preview: ", Toast.LENGTH_LONG);
                toast.show();
            }
            handlerThread.quitSafely();
        }, mHandler);
    }

    private void showBitmap() {
        // Imagen en bitmap
        screenshotImage.setImageBitmap(screenshotBitmap);
        screenshotLayout.setVisibility(View.VISIBLE);
        arUiButtons.setVisibility(View.GONE);
    }

    private void saveBitmapImage(){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/mindoBirdwatching");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        Log.i(TAG, "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            screenshotBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendBroadcast(new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }

    private void shareImage(){
        shareImageUri(saveImage());
    }

    private Uri saveImage() {
        //TODO - Should be processed in another thread
        File imagesFolder = new File(getCacheDir(), "mindoBirdwatching");
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            screenshotBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(this, "ec.com.mindo.mindobirdwatching.fileprovider", file);
            sendBroadcast(new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));

        } catch (IOException e) {
            Log.d(TAG, "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }

    private void shareImageUri(Uri uri){
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        startActivity(intent);
    }



    @Override
    public void onBackPressed() {
        if (showScreenshotUi) {
            closeScreenshotView();
        } else {
            super.onBackPressed();
        }
    }
}
