package ec.com.mindo.mindobirdwatching.poi;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class PoiMainAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<PoiVO> listStorage;
    private Context context;

    public PoiMainAdapter(Context context, List<PoiVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_poi_main, parent, false);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.textView);
            listViewHolder.imageInListView = (ImageView)convertView.findViewById(R.id.imageView);
            listViewHolder.descriptionInListView = (TextView) convertView.findViewById(R.id.descriptionTextView);
            listViewHolder.hotspotInCard = convertView.findViewById(R.id.hostpot_in_card_poi);
            listViewHolder.hotspotInCard.setVisibility(View.GONE);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        if(GlobalVar.getInstance().getNearbyPoi() == null){
            listViewHolder.hotspotInCard.setVisibility(View.GONE);
        } else if(GlobalVar.getInstance().getNearbyPoi().equals(listStorage.get(position).getPoiId())){
            listViewHolder.hotspotInCard.setVisibility(View.VISIBLE);
        } else {
            listViewHolder.hotspotInCard.setVisibility(View.GONE);
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getTitle());
        listViewHolder.descriptionInListView.setText(listStorage.get(position).getSubTitle());

        File dir = context.getDir(Constants.I_POI_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getImageName() + "." + listStorage.get(position).getExt();
        File mypath = new File(dir,filename);
        listViewHolder.imageInListView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
        TextView descriptionInListView;
        ImageView imageInListView;
        ImageView hotspotInCard;
    }
}
