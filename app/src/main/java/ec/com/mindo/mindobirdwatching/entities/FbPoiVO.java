package ec.com.mindo.mindobirdwatching.entities;

import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbPoiVO implements Serializable {

    private String id;
    private String imageName;
    private String ext;
    private Map<String, Double> location;
    private Map<String, String> enInfo;
    private Map<String, String> esInfo;
    private List<String> birdColRef;
    private String lastModifiedBy;
}
