package ec.com.mindo.mindobirdwatching.util;

public class Constants {

    public static final int CAMERA_PERMISSIONS = 1;
    public static final int EXTERNAL_WRITE_PERMISSIONS = 2;
    public static final int EXTERNAL_READ_PERMISSIONS = 3;
    public static final int LOCATION_PERMISSIONS = 4;

    public static final String I_ROUTES_FOLDER = "routes";
    public static final String I_POI_FOLDER = "poi";
    public static final String I_BIRDS_FOLDER = "birds";
    public static final String I_SOUNDS_FOLDER = "sounds";
    public static final String I_FAMILY_FOLDER = "families";

    public static final int DEFAULT_ZOOM = 16;

    public static final String POI_DIR = "/Tesis/poi/";
    public static final String ROOT_DIR = "/Tesis/";
    public static final String POI_FILE_NAME = "poi.json";
    public static final String VERSION_FILE_NAME = "version.json";
    public static final String FROM_DISCOVER = "fromDiscover";
    public static final String FAMILY_ID = "familyId";
    public static final String QUERY_STRING = "queryString";

    public static final String ENGLISH_LANGUAGE = "en";
    public static final String SPANISH_LANGUAGE = "es";

    public static final String ENGLISH_ROUTE = "/Tesis/en/";
    public static final String SPANISH_ROUTE = "/Tesis/es/";
    public static final String ROUTES_DIR = "/Tesis/routes/";
    public static final String ROUTES_FILE_NAME = "routes.json";
    public static final String BIRDS_DIR = "/Tesis/birds/";
    public static final String BIRDS_FILE_NAME = "birds.json";

    public static final String CHANNEL_ID = "mindoBirdwatching";
    public static final Integer NOTIF_ID = 1;
    public static final Integer NOTIF_ID_2 = 2;

    public static final String CHANNEL_NAME = "MindoBirdwatching channel";
    public static final String CHANNEL_DESCRIPTION = "MindoBirdwatching channel";

    public static final String SHOW_ALL_NOTIF = "showAlNotif";
    public static final String DOING_ROUTE = "doingRoute";
    public static final String CURR_ROUTE_ID = "currRouteId";
    public static final String FIRST_TIME = "firstTime";
    public static final String LANG_VAR = "Language";

    public static String getLanguageDir(String language){
        if(language.equals(ENGLISH_LANGUAGE)){
            return ENGLISH_ROUTE;
        } else if(language.equals(SPANISH_LANGUAGE)){
            return SPANISH_ROUTE;
        }
        return null;
    }
}
