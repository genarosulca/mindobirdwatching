package ec.com.mindo.mindobirdwatching.config;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.Locale;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.db.load.LoadDBActivity;
import ec.com.mindo.mindobirdwatching.language.LanguageChoseActivity;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class ConfigurationActivity extends BaseActivity {

    private static final String TAG = ConfigurationActivity.class.getSimpleName();
    public RelativeLayout loadingScreen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_configuration, (ViewGroup) findViewById(R.id.container));

        headerText.setText(getString(R.string.configuration));

//        setContentView(R.layout.activity_configuration);

        loadingScreen = findViewById(R.id.config_loading_screen);
        loadingScreen.setVisibility(View.GONE);

        getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));

        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
//        boolean showAllNotif = prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false);
        Boolean doingRoute = prefs.getBoolean(Constants.DOING_ROUTE, false);

        Switch allNotifSwitch = (Switch) findViewById(R.id.allNotifSwitch);
        allNotifSwitch.setChecked(prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false));
        if(allNotifSwitch.isChecked()){
            allNotifSwitch.setTrackTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
            allNotifSwitch.setThumbTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
        } else {
            allNotifSwitch.setTrackTintList(ColorStateList.valueOf(Color.LTGRAY));
            allNotifSwitch.setThumbTintList(ColorStateList.valueOf(Color.LTGRAY));
        }
        Log.i(TAG, allNotifSwitch.isChecked() + "");

        allNotifSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            // Get current status
//            boolean showAllNotif= prefs.getBoolean(Constants.SHOW_ALL_NOTIF, false);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(Constants.SHOW_ALL_NOTIF, allNotifSwitch.isChecked());
            editor.apply();
            if(allNotifSwitch.isChecked()){
                allNotifSwitch.setTrackTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
                allNotifSwitch.setThumbTintList(ColorStateList.valueOf(getColor(R.color.colorPrimary)));
                if(prefs.getBoolean(Constants.DOING_ROUTE, false)){
                    Util.deactivateNotifications(TAG, this);
                }
                Util.activateNotifications(TAG, this, true);
            } else {
                allNotifSwitch.setTrackTintList(ColorStateList.valueOf(Color.LTGRAY));
                allNotifSwitch.setThumbTintList(ColorStateList.valueOf(Color.LTGRAY));
                if(!prefs.getBoolean(Constants.DOING_ROUTE, false)){
                    Util.deactivateNotifications(TAG, this);
                }

            }
            loadingScreen.setVisibility(View.VISIBLE);
            Handler handler = new Handler();
            handler.postDelayed(() -> loadingScreen.setVisibility(View.GONE), 500);
        });

        TextView changeLanguageBtn = findViewById(R.id.changeLanguageBtn);
        changeLanguageBtn.setOnClickListener(view -> {
            showLanguageDialog();
        });

//        TextView boutBtn = findViewById(R.id.about_btn);
//        boutBtn.setOnClickListener(view -> showAboutDialog());
        this.setActive(R.id.nav_config);

        TextView currLangLabel = findViewById(R.id.curr_lang_label);
        if(prefs.getString(Constants.LANG_VAR, "").equals(Constants.ENGLISH_LANGUAGE)){ //
            currLangLabel.setText(R.string.eng_language);
        } else {
            currLangLabel.setText(R.string.esp_language);
        }

        Integer internalDbVersion = getInternalDBVersion();
        TextView dbVersionTxt = findViewById(R.id.database_version);
        dbVersionTxt.setText(getString(R.string.curr_db_version_txt) + " " + internalDbVersion.toString());
    }

    private void showLanguageDialog() {
        String[] languages = {getString(R.string.eng_language), getString(R.string.esp_language)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.change_language)
                .setItems(languages, (dialog, index) -> {
                    GlobalVar.getInstance().setPoiMainCol(null);
                    if(index == 0){
                        saveLocale(Constants.ENGLISH_LANGUAGE);
                    } else if(index == 1){
                        saveLocale(Constants.SPANISH_LANGUAGE);
                    }
                    goToMain();
                })
                .setPositiveButton(getString(R.string.close), (dialog, whichButton) -> {});

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnShowListener(arg0 ->
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        );

        alertDialog.show();
    }

    private void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.menu_about)
                .setMessage(R.string.about_message)
                .setPositiveButton(getString(R.string.close), (dialog, whichButton) -> {});

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnShowListener(arg0 ->
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        );

        alertDialog.show();
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.apply();

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.setLocale(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }

    private void goToMain(){
        Intent intent = new Intent(this, LoadDBActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private Integer getInternalDBVersion() {
        ConnectionSQLiteHelper versionConn = new ConnectionSQLiteHelper(this, DBConstants.DB_VERSION, null, 1);
        SQLiteDatabase sqLiteDatabase = versionConn.getReadableDatabase();
        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + DBConstants.TABLE_DBINFO, null);
            if (cursor == null) {
                return null;
            } else {
                cursor.moveToFirst();
                Integer version = cursor.getInt(0);
                cursor.close();
                return version;
            }
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            return null;
        } finally {
            versionConn.close();
        }
    }
}
