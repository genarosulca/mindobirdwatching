package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbBirdInfoVO implements Serializable {

    private String language;
    private String name;
    private String scientificName;
    private String lifetime;
    private String speed;
    private String weight;
    private String size;
    private String info;
    private String dangerInfo;
}
