package ec.com.mindo.mindobirdwatching.bird.library.description.videos;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryVideosAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<MediaResourceVO> listStorage;
    private Context context;

    public BirdLibraryVideosAdapter(Context context, List<MediaResourceVO> customizedListView) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if (convertView == null) {
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_video_gallery, parent, false);
            listViewHolder.imageInListView = (ImageView) convertView.findViewById(R.id.video_gallery_imageView);
            convertView.setTag(listViewHolder);
        } else {
            listViewHolder = (ViewHolder) convertView.getTag();
        }

        File dir = context.getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getThumnail() + "." + listStorage.get(position).getThumbnailExt();
        File mypath = new File(dir,filename);
        listViewHolder.imageInListView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        return convertView;
    }

    static class ViewHolder{
        ImageView imageInListView;
    }
}
