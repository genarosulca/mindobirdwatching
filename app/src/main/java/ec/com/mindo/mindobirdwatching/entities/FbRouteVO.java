package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbRouteVO implements Serializable {

    private String id;
    private String imageName;
    private String ext;
    private Long distance;
    private String distanceNotation;
    private Map<String, String> enInfo;
    private Map<String, String> esInfo;
    private List<String> birdColRef;
    private List<String> poiColRef;
    private String lastModifiedBy;
}
