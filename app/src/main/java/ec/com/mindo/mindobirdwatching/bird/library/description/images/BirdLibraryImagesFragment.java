package ec.com.mindo.mindobirdwatching.bird.library.description.images;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryImagesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bird_library_images, container, false);

        GridView gridview = (GridView) view.findViewById(R.id.images_gridview);

        BirdVO selectedBird = (BirdVO) getArguments().getSerializable(BirdLibraryDescriptionActivity.SELECTED_BIRD);

        BirdLibraryImagesAdapter customAdapter = new BirdLibraryImagesAdapter(view.getContext(), selectedBird.getImageGallery());
        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, v, position, id) -> {
            Intent intent = new Intent(v.getContext(), FullScreenImageActivity.class);
            intent.putExtra("position", position);
            intent.putExtra("allItems", (Serializable) selectedBird.getImageGallery());
            startActivity(intent);
        });

        Util.hideUnavaliableButtons(getActivity(),
                selectedBird,
                getArguments().getBoolean(Constants.FROM_DISCOVER)
        );

        FrameLayout header = getActivity().findViewById(R.id.bird_description_header);
        header.setVisibility(View.VISIBLE);

        return view;
    }

    private ArrayList<MediaResourceVO> getAllItemObject(BirdVO selectedBird){
        ArrayList<MediaResourceVO> images = new ArrayList<>();
        return images;
    }

}
