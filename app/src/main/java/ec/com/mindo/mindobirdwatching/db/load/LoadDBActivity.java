package ec.com.mindo.mindobirdwatching.db.load;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.ar.core.ArCoreApk;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ec.com.mindo.mindobirdwatching.MainActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.FbBirdVO;
import ec.com.mindo.mindobirdwatching.entities.FbFamilyVO;
import ec.com.mindo.mindobirdwatching.entities.FbPoiVO;
import ec.com.mindo.mindobirdwatching.entities.FbRouteVO;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class LoadDBActivity extends AppCompatActivity {

    private static String TAG = LoadDBActivity.class.getSimpleName();

    private static final String FB_VERSION_TABLE = "VERSION";
    private static final String FB_BIRD_TABLE = "BIRD";
    private static final String FB_POI_TABLE = "POI";
    private static final String FB_ROUTE_TABLE = "ROUTE";
    private static final String FB_FAMILIES_TABLE = "FAMILIES";

    private static final int POI_MEDIA_CREATION = 0;
    private static final int ROUTE_MEDIA_CREATION = 1;
    private static final int FAMILY_MEDIA_CREATION = 2;
    private static final int BIRDS_MEDIA_CREATION = 3;
    private static final int BIRD_IMAGES_CREATION = 4;
    private static final int BIRD_VIDEOS_CREATION = 5;
    private static final int BIRD_SOUNDS_CREATION = 6;

    private static final int QUANTITY = 10;
    private static final int CREATION = 11;

    private TextView loadingMessage;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private FirebaseFirestore db;
    private Integer createdQ;
    private int total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        loadLanguage();
        checkArSupport();
        askCameraPermission();
        createNotificationChannel();
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_db);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        loadingMessage = findViewById(R.id.loading_text);
        loadingMessage.setText(getResources().getString(R.string.loading));

        // TODO verificar si tiene conexion a internet via WiFi, si tiene via red sugerir que se conecte a Wifi
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return wifiConnected();
        } else {
            return (networkInfo != null && networkInfo.isConnected());
        }
    }

    private void showOnlineDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.verify_new_version));
        builder.setMessage(getString(R.string.verify_new_version_msg));
        // Si desea actualizar
        builder.setPositiveButton(R.string.yes, (dialog, whichButton) ->
                checkDbVersion()
        );
        // No desea actualizar
        if (canGoToMain()) {
            builder.setNegativeButton(R.string.no, (dialog, whichButton) -> goToMain());
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setOnShowListener(arg0 -> {
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        });

        alertDialog.show();
    }

    private boolean canGoToMain() {
        return getInternalVersion() != null;
    }

    private boolean wifiConnected() {
        Boolean withWifi = false;
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        for (Network network : connMgr.getAllNetworks()) {
            NetworkInfo networkInfo = connMgr.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                withWifi = networkInfo.isConnected();
            }
        }
        return withWifi;
    }


    private void checkDbVersion() {
        // Check si esta online
        if (!isOnline()) {
            // Abrir dialogo para decir que necesita una conexion genaro
            showOnlineDialog();
            return;
        }
        db = FirebaseFirestore.getInstance();
        db.collection(FB_VERSION_TABLE).document("GlR0etWMTv0uRkkaB0YF")
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    Boolean notifyChange = (Boolean) documentSnapshot.get("notifyChange");
                    Integer internalVersion = getInternalVersion();
                    if (internalVersion == null) {
                        // No hay version interna, crear bdd
                        storage = FirebaseStorage.getInstance("gs://mindo-birdwatching.appspot.com/");
                        storageRef = storage.getReference();
                        // Obtengo version
                        Long extVersion = (Long) documentSnapshot.get("version");
                        createOrUpdateDB(extVersion.intValue());
                        // TODO Paso a crear db por primera vez
//                        Log.i(TAG, "Se crea por primera vez BDD");
                    } else if(notifyChange){
                        // Obtengo version
                        Long extVersion = (Long) documentSnapshot.get("version");
                        if (internalVersion.equals(extVersion.intValue())) {
                            // Versiones coinciden, goToMain
                            goToMain();
                        } else {
                            // Versiones no coinciden
                            // TODO Actualizar version
                            storage = FirebaseStorage.getInstance("gs://mindo-birdwatching.appspot.com/");
                            storageRef = storage.getReference();
                            // TODO Mostrar dialog para confirmar creacion
                            showFbConfirmDialog(extVersion.intValue());
                        }
                    } else {
                        goToMain();
                    }
                });
    }

    private Integer getInternalVersion() {
        ConnectionSQLiteHelper versionConn = new ConnectionSQLiteHelper(this, DBConstants.DB_VERSION, null, 1);
        SQLiteDatabase sqLiteDatabase = versionConn.getReadableDatabase();
        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + DBConstants.TABLE_DBINFO, null);
            if (cursor == null) {
                return null;
            } else {
                cursor.moveToFirst();
                Integer version = cursor.getInt(0);
                cursor.close();
                return version;
            }
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
            return null;
        } finally {
            versionConn.close();
        }
    }

    private void goToMain() {
        Intent intent = new Intent(LoadDBActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void showFbConfirmDialog(Integer extVersion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.new_version_title);
        builder.setMessage(R.string.new_version_message);
        // Si desea actualizar
        builder.setPositiveButton(R.string.yes, (dialog, whichButton) ->
                createOrUpdateDB(extVersion)
        );
        // No desea actualizar
        builder.setNegativeButton(R.string.no, (dialog, whichButton) -> goToMain());
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setOnShowListener(arg0 -> {
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        });

        alertDialog.show();
    }

    private void createDbInfo() {
        //genaro
        db = FirebaseFirestore.getInstance();
        db.collection(FB_VERSION_TABLE).document("GlR0etWMTv0uRkkaB0YF")
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                            Long version = (Long) documentSnapshot.get("version");
                            ConnectionSQLiteHelper versionConn = new ConnectionSQLiteHelper(this, DBConstants.DB_VERSION, null, 1);
                            SQLiteDatabase sqLiteDatabase = versionConn.getWritableDatabase();
                            // Dropear tablas
                            sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_VERSION);
                            // Crear tablas
                            sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_VERSION);
                            // Creo la version
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DBConstants.FIELD_VERSION, version);
                            sqLiteDatabase.insert(DBConstants.TABLE_DBINFO, DBConstants.NOT_NULL_FIELDS, contentValues);
                            Log.i(TAG, "Creada tabla de version");
                            sqLiteDatabase.close();
                            versionConn.close();
                            goToMain();
                        }
                );
    }

    private void createOrUpdateDB(Integer version) {
        loadingMessage.setText(getResources().getString(R.string.downloading));
        createdQ = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        AsyncTask.execute(() -> {
//            createDbInfo(version);
            createPlaneInfo();
        });
    }

    private void createPlaneInfo() {
        readPoiInfo();
    }

    private void readPoiInfo() {
        // Load poi
        db.collection(FB_POI_TABLE)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            List<FbPoiVO> poiVOList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                FbPoiVO fbPoiVO = document.toObject(FbPoiVO.class);
                                poiVOList.add(fbPoiVO);
                            }
                            Log.i(TAG, "Se leyo " + poiVOList.size() + " POIs");
                            // Guardar en base
                            savePoiInfo(poiVOList, Constants.ENGLISH_LANGUAGE);
                            savePoiInfo(poiVOList, Constants.SPANISH_LANGUAGE);
                            createPoiMultimedia(poiVOList);
                        } else {
                            Log.e(TAG, "No se obtuvo ningun POI");
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    /**
     * Guarda en base POI con sus POIDetail
     *
     * @param items
     */
    private void savePoiInfo(List<FbPoiVO> items, String language) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(language), null, 1);
        SQLiteDatabase sqLiteDatabase = conn.getWritableDatabase();
        // Dropear tablas
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_POI);
        // Crear tablas
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_POI);
        // Guardar registros poi
        items.forEach(item -> {
            // Creo el poi
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_ID, item.getId());
            contentValues.put(DBConstants.FIELD_IMAGE_NAME, item.getImageName());
            contentValues.put(DBConstants.FIELD_EXT, item.getExt());
            contentValues.put(DBConstants.FIELD_LATITUDE, item.getLocation().get(DBConstants.FIELD_LATITUDE));
            contentValues.put(DBConstants.FIELD_LONGITUDE, item.getLocation().get(DBConstants.FIELD_LONGITUDE));
            if (language.equals(Constants.ENGLISH_LANGUAGE)) {
                item.getEnInfo().forEach(contentValues::put);
            } else if (language.equals(Constants.SPANISH_LANGUAGE)) {
                item.getEsInfo().forEach(contentValues::put);
            }
            String relatedBirds = item.getBirdColRef().toString().replaceAll("[\\[\\]\\s]", "");
            contentValues.put(DBConstants.FIELD_RELATED_BIRDS, relatedBirds);
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_POI, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_POI + " " + resultId);
        });
        sqLiteDatabase.close();
    }

    private void createPoiMultimedia(List<FbPoiVO> poiVOList) {
        // Usamos el directorio si existe, si no, se crea
        String folderName = Constants.I_POI_FOLDER;
        File dir = this.getDir(folderName, Context.MODE_PRIVATE); //Creating an internal dir
        List<String> fileNames = poiVOList.stream()
                .map(vo -> vo.getImageName() + "." + vo.getExt()).collect(Collectors.toList());
        readRouteInfo();
    }

    private void readRouteInfo() {
        db.collection(FB_ROUTE_TABLE)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            List<FbRouteVO> routeVOList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                FbRouteVO fbRouteVO = document.toObject(FbRouteVO.class);
                                routeVOList.add(fbRouteVO);
                            }
                            Log.i(TAG, "Se leyo " + routeVOList.size() + " rutas");
                            // Guardar en base
                            saveRouteInfo(routeVOList, Constants.ENGLISH_LANGUAGE);
                            saveRouteInfo(routeVOList, Constants.SPANISH_LANGUAGE);
                            createRouteMultimedia(routeVOList);
                        } else {
                            Log.e(TAG, "No se obtuvo ningun Route");
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    private void saveRouteInfo(List<FbRouteVO> items, String language) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(language), null, 1);
        SQLiteDatabase sqLiteDatabase = conn.getWritableDatabase();
        // Dropear tablas
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_ROUTE);
        // Crear tablas
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_ROUTE);
        // Guardar registros de rutas
        items.forEach(item -> {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_ID, item.getId());
            contentValues.put(DBConstants.FIELD_IMAGE_NAME, item.getImageName());
            contentValues.put(DBConstants.FIELD_EXT, item.getExt());
            contentValues.put(DBConstants.FIELD_DISTANCE, item.getDistance());
            contentValues.put(DBConstants.FIELD_DISTANCE_NOTATION, item.getDistanceNotation());
            if (language.equals(Constants.ENGLISH_LANGUAGE)) {
                item.getEnInfo().forEach(contentValues::put);
            } else if (language.equals(Constants.SPANISH_LANGUAGE)) {
                item.getEsInfo().forEach(contentValues::put);
            }
            // TODO Obtener los related birds
            // Obtengo los POIs
            String dirtyRelatedBirds = getRouteBirds(item.getPoiColRef());
            String relatedPois = item.getPoiColRef().toString().replaceAll("[\\[\\]\\s]", "");
            contentValues.put(DBConstants.FIELD_RELATED_BIRDS, dirtyRelatedBirds.replaceAll("[\\[\\]\\s]", ""));
            contentValues.put(DBConstants.FIELD_RELATED_POIS, relatedPois);
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_ROUTE, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_ROUTE + " " + resultId);
        });
        sqLiteDatabase.close();
    }

    private void createRouteMultimedia(List<FbRouteVO> routeVOList) {
        // Usamos el directorio si existe, si no, se crea
        String folderName = Constants.I_ROUTES_FOLDER;
        File dir = this.getDir(folderName, Context.MODE_PRIVATE); //Creating an internal dir
        List<String> fileNames = routeVOList.stream()
                .map(vo -> vo.getImageName() + "." + vo.getExt()).collect(Collectors.toList());
        readFammilyInfo();
    }

    private void readFammilyInfo() {
        db.collection(FB_FAMILIES_TABLE)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (task.getResult() != null) {
                            List<FbFamilyVO> familyVoList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                FbFamilyVO fbFamilyVO = document.toObject(FbFamilyVO.class);
                                familyVoList.add(fbFamilyVO);
                            }
                            Log.i(TAG, "Se leyo " + familyVoList.size() + " Families");
                            // crear en sqlite
                            saveFamilies(Constants.ENGLISH_LANGUAGE, familyVoList);
                            saveFamilies(Constants.SPANISH_LANGUAGE, familyVoList);
                            createFamilyMultimedia(familyVoList);
                        } else {
                            Log.e(TAG, "No se obtuvo ningun Route");
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    private void saveFamilies(String language, List<FbFamilyVO> familyVoList) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(language), null, 1);
        SQLiteDatabase sqLiteDatabase = conn.getWritableDatabase();
        // Dropear tablas
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_FAMILY);
        // Tablas de birds
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_BIRD);
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_BIRD_IMAGE);
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_BIRD_VIDEO);
        sqLiteDatabase.execSQL(DBConstants.DROP_TABLE_BIRD_SOUND);
        // Crear tablas
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_FAMILY);
        // tablas de birds
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_BIRD);
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_BIRD_IMAGE);
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_BIRD_VIDEO);
        sqLiteDatabase.execSQL(DBConstants.CREATE_TABLE_BIRD_SOUND);
        // Guardar registros family
        familyVoList.forEach(item -> {
            // Creo familia
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_ID, item.getId());
            contentValues.put(DBConstants.FIELD_IMAGE_NAME, item.getImageName());
            contentValues.put(DBConstants.FIELD_EXT, item.getExt());
            contentValues.put(DBConstants.FIELD_SCIENTIFIC_NAME, item.getScientificName());
            if (language.equals(Constants.ENGLISH_LANGUAGE)) {
                item.getEnInfo().forEach(contentValues::put);
            } else if (language.equals(Constants.SPANISH_LANGUAGE)) {
                item.getEsInfo().forEach(contentValues::put);
            }
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_FAMILY, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_FAMILY + " " + resultId);
        });
        sqLiteDatabase.close();
    }

    private void createFamilyMultimedia(List<FbFamilyVO> familyVoList) {
        // Usamos el directorio si existe, si no, se crea
        String folderName = Constants.I_FAMILY_FOLDER;
        File dir = this.getDir(folderName, Context.MODE_PRIVATE);
        List<String> fileNames = familyVoList.stream()
                .map(vo -> vo.getImageName() + "." + vo.getExt()).collect(Collectors.toList());
        readBirds(familyVoList, 0);
    }

    private void readBirds(List<FbFamilyVO> familyVoList, int birdIndex) {
        if (birdIndex < familyVoList.size()) {
            db.collection(FB_BIRD_TABLE).whereEqualTo(DBConstants.FIELD_FAMILY_ID, familyVoList.get(birdIndex).getId())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            if (task.getResult() != null) {
                                List<FbBirdVO> birdVOList = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    FbBirdVO fbBirdVO = document.toObject(FbBirdVO.class);
                                    birdVOList.add(fbBirdVO);
                                }
                                // crear sqlite
                                saveBirdInfo(birdVOList, Constants.ENGLISH_LANGUAGE);
                                saveBirdInfo(birdVOList, Constants.SPANISH_LANGUAGE);
                                createBirdMultimedia(birdVOList, familyVoList, birdIndex);
                            } else {
                                Log.e(TAG, "No se obtuvo ningun Route");
                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    });
        } else {
            // Acaba de guardar birds
            getTotalQuantity();
            saveMultimedia(POI_MEDIA_CREATION);
        }
    }

    private void saveBirdInfo(List<FbBirdVO> items, String language) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(language), null, 1);
        SQLiteDatabase sqLiteDatabase = conn.getWritableDatabase();
        items.forEach(item -> {
            // Creo image gallery
            if (item.getImageGallery() != null) {
                createImageGallery(sqLiteDatabase, item.getImageGallery(), item.getId());
            }
            // Creo video gallery
            if (item.getVideoGallery() != null) {
                createVideoGallery(sqLiteDatabase, item.getVideoGallery(), item.getId());
            }
            // Creo sound gallery
            if (item.getSoundGallery() != null) {
                createSoundGallery(sqLiteDatabase, item.getSoundGallery(), item.getId());
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_ID, item.getId());
            contentValues.put(DBConstants.FIELD_FAMILY_ID, item.getFamilyId());
            contentValues.put(DBConstants.FIELD_IMAGE_NAME, item.getImageName());
            contentValues.put(DBConstants.FIELD_EXT, item.getExt());
            contentValues.put(DBConstants.FIELD_IN_DANGER, item.getInDanger());
            contentValues.put(DBConstants.FIELD_ACTIVATE_AR, item.getActivateAr());
            // Guarda la info
            if (language.equals(Constants.ENGLISH_LANGUAGE)) {
                item.getEnInfo().forEach(contentValues::put);
            } else if (language.equals(Constants.SPANISH_LANGUAGE)) {
                item.getEsInfo().forEach(contentValues::put);
            }
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_BIRD, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_BIRD + " " + resultId);
        });
        sqLiteDatabase.close();
    }

    private void createImageGallery(SQLiteDatabase sqLiteDatabase, List<Map<String, String>> imageGallery, String birdId) {
        imageGallery.forEach(item -> {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_BIRD_ID, birdId);
            item.forEach(contentValues::put);
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_BIRD_IMAGE, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_BIRD_IMAGE + " " + resultId);
        });
    }

    private void createVideoGallery(SQLiteDatabase sqLiteDatabase, List<Map<String, String>> videoGallery, String birdId) {
        videoGallery.forEach(item -> {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_BIRD_ID, birdId);
            item.forEach(contentValues::put);
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_BIRD_VIDEO, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_BIRD_VIDEO + " " + resultId);
        });
    }

    private void createSoundGallery(SQLiteDatabase sqLiteDatabase, List<Map<String, String>> soundGallery, String birdId) {
        soundGallery.forEach(item -> {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBConstants.FIELD_BIRD_ID, birdId);
            item.forEach(contentValues::put);
            Long resultId = sqLiteDatabase.insert(DBConstants.TABLE_BIRD_SOUND, DBConstants.NOT_NULL_FIELDS, contentValues);
            Log.i(TAG, "Creado " + DBConstants.TABLE_BIRD_SOUND + " " + resultId);
        });
    }

    private void createBirdMultimedia(List<FbBirdVO> birdVOList, List<FbFamilyVO> familyVoList, int birdIndex) {

        List<String> fileNames = birdVOList.stream()
                .map(vo -> vo.getImageName() + "." + vo.getExt()).collect(Collectors.toList());
        birdVOList.forEach(vo -> {
            fileNames.add(vo.getImageName() + "." + vo.getExt());
            if (vo.getImageGallery() != null) {
                vo.getImageGallery().forEach(map -> {
                    fileNames.add(map.get(DBConstants.FIELD_IMAGE_NAME) + "." + map.get(DBConstants.FIELD_EXT));
                    fileNames.add(map.get(DBConstants.FIELD_THUMBNAIL) + "." + map.get(DBConstants.FIELD_THUMBNAIL_EXT));
                });
            }
            if (vo.getVideoGallery() != null) {
                vo.getVideoGallery().forEach(map -> {
                    fileNames.add(map.get(DBConstants.FIELD_VIDEO_NAME) + "." + map.get(DBConstants.FIELD_EXT));
                    fileNames.add(map.get(DBConstants.FIELD_THUMBNAIL) + "." + map.get(DBConstants.FIELD_THUMBNAIL_EXT));
                });
            }
        });
        crateBirdSounds(birdVOList, familyVoList, birdIndex);
    }

    private void crateBirdSounds(List<FbBirdVO> birdVOList, List<FbFamilyVO> familyVoList, int birdIndex) {
        String folderName = Constants.I_SOUNDS_FOLDER;
        File dir = this.getDir(folderName, Context.MODE_PRIVATE);
        List<String> fileNames = new ArrayList<>();
        birdVOList.forEach(vo -> {
            if (vo.getSoundGallery() != null) {
                vo.getSoundGallery().forEach(map -> {
                    fileNames.add(map.get(DBConstants.FIELD_SOUND_NAME) + "." + map.get(DBConstants.FIELD_EXT));
                });
            }
        });
        readBirds(familyVoList, birdIndex + 1);
    }

    private void askCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    Constants.CAMERA_PERMISSIONS);
        } else {
            askExternalStoragePermission();
        }
    }

    private void askExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.EXTERNAL_WRITE_PERMISSIONS);
        } else {
            askLocationPermission();
        }
    }

    private void askLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.LOCATION_PERMISSIONS);
        } else {
            checkDbVersion();
        }
    }

    public void loadLanguage() {
        //Load language
        String langPref = "Language";
        SharedPreferences prefs = this.getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        if (language.equals("")) {
            if (Locale.getDefault().getLanguage().equals(Constants.ENGLISH_LANGUAGE) ||
                    Locale.getDefault().getLanguage().equals(Constants.SPANISH_LANGUAGE)) {
                language = Locale.getDefault().getLanguage();

            } else {
                language = Constants.ENGLISH_LANGUAGE;
            }
        }
        GlobalVar.getInstance().setCurrentLanguage(language);
        if (!language.isEmpty()) {
            //Change language
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            android.content.res.Configuration config = new android.content.res.Configuration();
            config.setLocale(locale);
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public void checkArSupport() {
        ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
        if (availability.isTransient()) {
            // Re-query at 5Hz while compatibility is checked in the background.
            new Handler().postDelayed(this::checkArSupport, 200);
        }
        if (availability.isSupported()) {
            GlobalVar.getInstance().setArSupported(Boolean.TRUE);
            // indicator on the button.
        } else { // Unsupported or unknown.
            GlobalVar.getInstance().setArSupported(Boolean.FALSE);
        }
    }

    private void showCameraPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.info_title)
                .setMessage(R.string.camera_permission)
                .setPositiveButton(R.string.dialog_accept, (dialog, whichButton) -> askCameraPermission());

        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private void showWriteExternalPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.info_title)
                .setMessage(R.string.write_external_permission)
                .setPositiveButton(R.string.dialog_accept, (dialog, whichButton) -> askExternalStoragePermission());

        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private void showLocationPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.info_title)
                .setMessage(R.string.location_permission)
                .setPositiveButton(R.string.dialog_accept, (dialog, whichButton) -> askLocationPermission());

        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.CAMERA_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askExternalStoragePermission();
                } else {
                    showCameraPermissionDialog();
                }
                return;
            }
            case Constants.EXTERNAL_WRITE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askLocationPermission();
                } else {
                    showWriteExternalPermissionDialog();
                }
                return;
            }
            case Constants.LOCATION_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkDbVersion();
                } else {
                    showLocationPermissionDialog();
                }
                return;
            }
        }
    }

    private void saveMultimedia(int section) {
        String folderName;
        File dir;
        switch (section) {
            case (POI_MEDIA_CREATION):
                folderName = Constants.I_POI_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getPoiImagesNames(), 0, dir, folderName, POI_MEDIA_CREATION);
                break;
            case (ROUTE_MEDIA_CREATION):
                folderName = Constants.I_ROUTES_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getRoutesImagesNames(), 0, dir, folderName, ROUTE_MEDIA_CREATION);
                break;
            case (FAMILY_MEDIA_CREATION):
                folderName = Constants.I_FAMILY_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getFamiliesImagesNames(), 0, dir, folderName, FAMILY_MEDIA_CREATION);
                break;
            case (BIRDS_MEDIA_CREATION):
                folderName = Constants.I_BIRDS_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getBirdsMenuImagesNames(), 0, dir, folderName, BIRDS_MEDIA_CREATION);
                break;
            case (BIRD_IMAGES_CREATION):
                folderName = Constants.I_BIRDS_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getBirdsImagesNames(), 0, dir, folderName, BIRD_IMAGES_CREATION);
                break;
            case (BIRD_VIDEOS_CREATION):
                folderName = Constants.I_BIRDS_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getBirdsVideosNames(), 0, dir, folderName, BIRD_VIDEOS_CREATION);
                break;
            case (BIRD_SOUNDS_CREATION):
                folderName = Constants.I_SOUNDS_FOLDER;
                dir = this.getDir(folderName, Context.MODE_PRIVATE);
                saveImages(getBirdsSoundsNames(), 0, dir, folderName, BIRD_SOUNDS_CREATION);
                break;
        }

    }

    private void saveImages(List<String> fileNames, int index, File dir, String folderName, int section) {
        if (index < fileNames.size()) {
            File birdFile = new File(dir, fileNames.get(index));
            StorageReference birdRef = storageRef.child(folderName + "/" + fileNames.get(index));
            birdRef.getFile(birdFile).addOnSuccessListener(taskSnapshot -> {
                // File has been created
                createdQ = createdQ + 1;
                Message msg = new Message();
                msg.what = CREATION;
                mHandler.sendMessage(msg);
                Log.i(TAG, "Descargado " + fileNames.get(index));
                saveImages(fileNames, index + 1, dir, folderName, section);
            }).addOnFailureListener(exception -> {
                // Handle any errors
                Log.e(TAG, "Error al descargar " + fileNames.get(index));
                // TODO delete version and close app
            });
        } else {
            switch (section) {
                case (POI_MEDIA_CREATION):
                    saveMultimedia(ROUTE_MEDIA_CREATION);
                    break;
                case (ROUTE_MEDIA_CREATION):
                    saveMultimedia(FAMILY_MEDIA_CREATION);
                    break;
                case (FAMILY_MEDIA_CREATION):
                    saveMultimedia(BIRDS_MEDIA_CREATION);
                    break;
                case (BIRDS_MEDIA_CREATION):
                    saveMultimedia(BIRD_IMAGES_CREATION);
                    break;
                case (BIRD_IMAGES_CREATION):
                    saveMultimedia(BIRD_VIDEOS_CREATION);
                    break;
                case (BIRD_VIDEOS_CREATION):
                    saveMultimedia(BIRD_SOUNDS_CREATION);
                    break;
                case (BIRD_SOUNDS_CREATION):
                    createDbInfo();
                    break;
            }
        }

    }

    private String getRouteBirds(List<String> relatedPois) {
        if(relatedPois.isEmpty()){
            return "";
        } else {
            ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
            SQLiteDatabase db = conn.getReadableDatabase();
            Set<String> relatedBirds = new HashSet<>();
            // TODO Falta el WHERE
            String[] parameters = relatedPois.toArray(new String[relatedPois.size()]);
            Cursor cursor = db.rawQuery("SELECT " + DBConstants.FIELD_RELATED_BIRDS + " FROM " + DBConstants.TABLE_POI + " WHERE " +
                    DBConstants.FIELD_ID + " IN (" + Util.makePlaceholders(parameters.length) + ")", parameters);
            while (cursor.moveToNext()) {
                String[] relatedPoiBirds = cursor.getString(0).split(",");
                relatedBirds.addAll(Arrays.asList(relatedPoiBirds));
            }
            return relatedBirds.toString();
        }
    }

    private List<String> getPoiImagesNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_POI, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
        }
        return fileNames;
    }

    private List<String> getRoutesImagesNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_ROUTE, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
        }
        return fileNames;
    }

    private List<String> getFamiliesImagesNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_FAMILY, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
        }
        return fileNames;
    }

    private List<String> getBirdsMenuImagesNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_BIRD, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
        }
        return fileNames;
    }

    private List<String> getBirdsImagesNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT, DBConstants.FIELD_THUMBNAIL, DBConstants.FIELD_THUMBNAIL_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_BIRD_IMAGE, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
            fileNames.add(cursor.getString(2) + "." + cursor.getString(3));
        }
        return fileNames;
    }

    private List<String> getBirdsVideosNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_VIDEO_NAME, DBConstants.FIELD_EXT, DBConstants.FIELD_THUMBNAIL, DBConstants.FIELD_THUMBNAIL_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_BIRD_VIDEO, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
            fileNames.add(cursor.getString(2) + "." + cursor.getString(3));
        }
        return fileNames;
    }

    private List<String> getBirdsSoundsNames() {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<String> fileNames = new ArrayList<>();
        String[] selectedFields = {DBConstants.FIELD_SOUND_NAME, DBConstants.FIELD_EXT};
        Cursor cursor = db.query(DBConstants.TABLE_BIRD_SOUND, selectedFields, null, null, null, null, null);
        while (cursor.moveToNext()) {
            fileNames.add(cursor.getString(0) + "." + cursor.getString(1));
        }
        return fileNames;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(Constants.CHANNEL_ID,
                    Constants.CHANNEL_NAME, importance);
            channel.setDescription(Constants.CHANNEL_DESCRIPTION);
            channel.setShowBadge(true);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void getTotalQuantity() {
        int total = 0;
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor;
//        POI
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_POI, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0);
//        ROUTES
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_ROUTE, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0);
//        FAMILIES
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_FAMILY, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0);
//        BIRDS
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_ID + ") FROM " + DBConstants.TABLE_BIRD, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0);
//        IMAGES
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_EXT + ") FROM " + DBConstants.TABLE_BIRD_IMAGE, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0) * 2;
//        VIDEOS
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_EXT + ") FROM " + DBConstants.TABLE_BIRD_VIDEO, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0) * 2;
//        SOUNDS
        cursor = db.rawQuery("SELECT count(" + DBConstants.FIELD_EXT + ") FROM " + DBConstants.TABLE_BIRD_SOUND, null);
        cursor.moveToFirst();
        total = total + cursor.getInt(0);

        cursor.close();
        Message msg = new Message();
        msg.what = QUANTITY;
        msg.arg1 = total;
        mHandler.sendMessage(msg);

    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case QUANTITY:
                    total = msg.arg1;
                    break;
                case CREATION:
                    changePercentage();
                    break;
            }
        }
    };

    private void changePercentage() {
        int percentage = (createdQ * 100) / total;
        Log.i(TAG, "TOTAL " + total + " CREADO " + createdQ);
        loadingMessage.setText(getResources().getString(R.string.downloading) + " " + percentage + " %");
    }

}
