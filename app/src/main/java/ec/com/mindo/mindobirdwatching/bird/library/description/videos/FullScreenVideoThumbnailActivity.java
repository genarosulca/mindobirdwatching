package ec.com.mindo.mindobirdwatching.bird.library.description.videos;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import ec.com.mindo.mindobirdwatching.R;

public class FullScreenVideoThumbnailActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ViewPager viewPager = findViewById(R.id.image_viewPager);
        FullScreenVideoThumbnailAdapter adapter = new FullScreenVideoThumbnailAdapter(this,
                getIntent().getIntegerArrayListExtra("allItems"));
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(getIntent().getExtras().getInt("position"));
    }
}
