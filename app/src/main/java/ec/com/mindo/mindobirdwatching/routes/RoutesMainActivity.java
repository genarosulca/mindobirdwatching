package ec.com.mindo.mindobirdwatching.routes;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.RouteVO;
import ec.com.mindo.mindobirdwatching.routes.description.RoutesDescriptionActivity;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;

public class RoutesMainActivity extends BaseActivity {

    private List<RouteVO> routesCol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_routes_main, findViewById(R.id.container));

        getAllItems();

        GridView gridview = this.findViewById(R.id.routes_gridview);

        RoutesMainAdapter customAdapter = new RoutesMainAdapter(this, routesCol);
        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            //loadingScreen.setVisibility(View.VISIBLE);
            Intent intent = new Intent(view.getContext(), RoutesDescriptionActivity.class);
            intent.putExtra("selectedRoute", routesCol.get(position));
            startActivity(intent);
        });

        this.setActive(R.id.nav_routes);
        headerText.setText(R.string.routes);
    }

    private void getAllItems(){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        routesCol = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_ROUTE,null);
        while (cursor.moveToNext()){
            RouteVO route = new RouteVO(
                    cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8)
            );
            routesCol.add(route);
        }
        cursor.close();
        db.close();
        //loadingScreen.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //loadingScreen.setVisibility(View.GONE);
    }
}
