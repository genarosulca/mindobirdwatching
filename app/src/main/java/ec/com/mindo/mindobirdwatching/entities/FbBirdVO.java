package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbBirdVO implements Serializable {

    private Boolean activateAr;
    private String id;
    private String familyId;
    private String imageName;
    private String ext;
    private Boolean inDanger;
    private Map<String, String> enInfo;
    private Map<String, String> esInfo;
    private List<Map<String, String>> imageGallery;
    private List<Map<String, String>> videoGallery;
    private List<Map<String, String>> soundGallery;
    private String lastModifiedBy;
    //    private List<MediaResourceVO> imageGallery;
//    private List<MediaResourceVO> videoGallery;
}
