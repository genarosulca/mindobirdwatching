package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FbRouteInfoVO implements Serializable {

    private String name;
}
