package ec.com.mindo.mindobirdwatching.bird.library.description.danger;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryDangerFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedObj = inflater.inflate(R.layout.fragment_bird_library_danger, container, false);

        BirdVO selectedBird = (BirdVO) getArguments().getSerializable(BirdLibraryDescriptionActivity.SELECTED_BIRD);

        TextView description = inflatedObj.findViewById(R.id.danger_descrption);
        description.setText(selectedBird.getDangerInfo());


        Util.hideUnavaliableButtons(getActivity(),
                selectedBird,
                getArguments().getBoolean(Constants.FROM_DISCOVER)
        );

        return inflatedObj;
    }
}
