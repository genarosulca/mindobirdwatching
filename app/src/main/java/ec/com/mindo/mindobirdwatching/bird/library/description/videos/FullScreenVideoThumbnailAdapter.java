package ec.com.mindo.mindobirdwatching.bird.library.description.videos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.List;

import ec.com.mindo.mindobirdwatching.R;

public class FullScreenVideoThumbnailAdapter extends PagerAdapter {

    private List<Integer> thumbnails;
    private LayoutInflater inflater;
    private Context context;
    private Activity activity;


    public FullScreenVideoThumbnailAdapter(Activity activity, List<Integer> thumbnails) {
        this.context = activity.getBaseContext();
        this.thumbnails = thumbnails;
        this.activity = activity;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return thumbnails.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.listview_slide_image, view, false);

        final PhotoView imageView = (PhotoView) imageLayout
                .findViewById(R.id.full_screen_image);

        imageView.setOnPhotoTapListener((v, x, y) -> {
            // Play video
            playVideo();
        });

        Glide.with(context)
                .load(thumbnails.get(position))
                .into(imageView);

        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void  playVideo(){
        Intent intent = new Intent(context, FullScreenVideoPlayer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
