package ec.com.mindo.mindobirdwatching.bird.library;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.MainActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.util.BirdUtils;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryMainActivity extends BaseActivity {

    private List<BirdVO> birdLibraryCol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();

        inflater.inflate(R.layout.activity_birdlibrary_main, (ViewGroup) findViewById(R.id.container));

        GridView gridview = (GridView) this.findViewById(R.id.birdlibrary_main_gridview);

        String queryString = getIntent().getExtras().getString(Constants.QUERY_STRING);

        BirdLibraryMainAdapter customAdapter;

        boolean fromDiscover = getIntent().getExtras().getBoolean(Constants.FROM_DISCOVER);
        String familyId = getIntent().getExtras().getString(Constants.FAMILY_ID);

        if (fromDiscover) {
            if (GlobalVar.getInstance().getNearbyPoi() == null) {
                //go back to main activity
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
            PoiVO currentPoi = GlobalVar.getInstance().getPoiMainCol().stream()
                    .filter(poiMainObject -> poiMainObject.getPoiId().equals(GlobalVar.getInstance().getNearbyPoi()))
                    .findFirst().orElse(null);
            if (currentPoi == null) {
                //go back to main activity
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
            // Buscar los birds que son solo para el poi
            birdLibraryCol = getAllBirds(currentPoi.getRelatedBirds(), null);
        } else {
            searchView.setVisibility(View.VISIBLE);
            if(queryString == null){
                birdLibraryCol = getAllBirds(null, familyId);
            } else {
                birdLibraryCol = getFilteredBirds(queryString);
            }

        }
        customAdapter = new BirdLibraryMainAdapter(this, birdLibraryCol, fromDiscover);

        headerText.setText(getString(R.string.birdspecies));

        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            //loadingScreen.setVisibility(View.VISIBLE);
            Intent intent = new Intent(view.getContext(), BirdLibraryDescriptionActivity.class);
            BirdVO selectedBird = birdLibraryCol.get(position);
            selectedBird.setFamilyScientificName(getScientificFamilyName(selectedBird.getFamilyId()));
            selectedBird.setSoundsGallery(BirdUtils.getAllSounds(selectedBird.getBirdId(), this));
            selectedBird.setVideoGallery(BirdUtils.getAllVideos(selectedBird.getBirdId(), this));
            selectedBird.setImageGallery(BirdUtils.getAllImages(selectedBird.getBirdId(), this));
            intent.putExtra("selectedBird", selectedBird);
            intent.putExtra(Constants.FROM_DISCOVER, fromDiscover);
            startActivity(intent);
        });
        this.setActive(R.id.nav_bird_library);
    }

    private String getScientificFamilyName(String familyId){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();

        String[] selectedFields = {DBConstants.FIELD_SCIENTIFIC_NAME};
        String[] parameters = {familyId};
        String where = DBConstants.FIELD_ID + "=?";
        Cursor cursor = db.query(DBConstants.TABLE_FAMILY, selectedFields, where, parameters, null, null, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }



    private List<BirdVO> getFilteredBirds(String queryString) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD + " WHERE LOWER(" +
                DBConstants.FIELD_NAME + ") LIKE '%" + queryString + "%'", null);
        return armarListas(cursor);
    }

    private List<BirdVO> getAllBirds(String relatedBirds, String familyId){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(this, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        Cursor cursor;
        if(familyId != null){
            String[] parameters = {familyId};
            cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD + " WHERE " +
                    DBConstants.FIELD_FAMILY_ID + " = ?", parameters);
        } else{
            // Comes from discover
            String[] parameters = relatedBirds.split(",");
            cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD + " WHERE " +
                    DBConstants.FIELD_ID + " IN (" + Util.makePlaceholders(parameters.length) + ") " +
                    "ORDER BY " + DBConstants.FIELD_ACTIVATE_AR + " DESC" , parameters);
        }
        return armarListas(cursor);
    }

    private List<BirdVO> armarListas(Cursor cursor) {
        List<BirdVO> birdNames = new ArrayList<>();
        while (cursor.moveToNext()){
            birdNames.add(
                    new BirdVO(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getInt(4) != 0,
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getInt(13) != 0
                    )
            );
        }
        //loadingScreen.setVisibility(View.GONE);
        return birdNames;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //loadingScreen.setVisibility(View.GONE);
    }
}
