package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FamilyVO implements Serializable {

    private String familyId;
    private String ext;
    private String imageName;
    private String name;
    private String scientificName;
    private List<BirdVO> birdCol;

    public FamilyVO(String familyId, String imageName, String ext, String name, String scientificName) {
        this.familyId = familyId;
        this.imageName = imageName;
        this.ext = ext;
        this.name = name;
        this.scientificName = scientificName;
    }
}
