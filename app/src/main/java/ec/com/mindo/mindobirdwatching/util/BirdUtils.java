package ec.com.mindo.mindobirdwatching.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.MediaResourceVO;

    public class BirdUtils {

    public static List<MediaResourceVO> getAllImages(String birdId, Context context){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(context, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<MediaResourceVO> birdImages = new ArrayList<>();

        String[] parameters = {birdId};
        String[] selectedFields = {DBConstants.FIELD_IMAGE_NAME, DBConstants.FIELD_EXT, DBConstants.FIELD_THUMBNAIL, DBConstants.FIELD_THUMBNAIL_EXT, DBConstants.FIELD_AUTHOR};
        String where = DBConstants.FIELD_BIRD_ID + "=?";

        Cursor cursor = db.query(DBConstants.TABLE_BIRD_IMAGE, selectedFields, where, parameters, null, null, null);

        while (cursor.moveToNext()){
            birdImages.add(
                    new MediaResourceVO(
                            null,
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4)
                    )
            );
        }
        return birdImages;
    }

    public static List<MediaResourceVO> getAllVideos(String birdId, Context context){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(context, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<MediaResourceVO> birdImages = new ArrayList<>();

        String[] parameters = {birdId};
        String[] selectedFields = {DBConstants.FIELD_VIDEO_NAME, DBConstants.FIELD_EXT, DBConstants.FIELD_THUMBNAIL, DBConstants.FIELD_THUMBNAIL_EXT, DBConstants.FIELD_AUTHOR};
        String where = DBConstants.FIELD_BIRD_ID + "=?";

        Cursor cursor = db.query(DBConstants.TABLE_BIRD_VIDEO, selectedFields, where, parameters, null, null, null);

        while (cursor.moveToNext()){
            birdImages.add(
                    new MediaResourceVO(
                            null,
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4)
                    )
            );
        }
        return birdImages;
    }

    public static List<MediaResourceVO> getAllSounds(String birdId, Context context){
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(context, DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        List<MediaResourceVO> birdSounds = new ArrayList<>();

        String[] parameters = {birdId};
        String[] selectedFields = {DBConstants.FIELD_SOUND_NAME, DBConstants.FIELD_EXT, DBConstants.FIELD_AUTHOR};
        String where = DBConstants.FIELD_BIRD_ID + "=?";

        Cursor cursor = db.query(DBConstants.TABLE_BIRD_SOUND, selectedFields, where, parameters, null, null, null);
//        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD_SOUND, null);
        while (cursor.moveToNext()){
            birdSounds.add(
                    new MediaResourceVO(
                            null,
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2)
                    )
            );
        }
        return birdSounds;
    }
}
