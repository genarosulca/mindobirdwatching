package ec.com.mindo.mindobirdwatching.bird.library;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryMainAdapter extends BaseAdapter {

    private LayoutInflater layoutinflater;
    private List<BirdVO> listStorage;
    private Context context;
    private Boolean fromDiscover;

    public BirdLibraryMainAdapter(Context context, List<BirdVO> customizedListView, Boolean fromDiscover) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = customizedListView;
        this.fromDiscover = fromDiscover;
    }


    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder listViewHolder;
        if(convertView == null){
            listViewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.listview_birlibrary_main, parent, false);
            listViewHolder.imageInListView = (ImageView)convertView.findViewById(R.id.birdlibrary_main_imageView);
//            listViewHolder.imageInListView.setScaleType(Matrix.ScaleToFit.START);
            listViewHolder.arInCard = (ImageView)convertView.findViewById(R.id.ar_in_card);
            listViewHolder.textInListView = (TextView)convertView.findViewById(R.id.birdlibrary_main_titleTextView);
            convertView.setTag(listViewHolder);
        }else{
            listViewHolder = (ViewHolder)convertView.getTag();
        }

        listViewHolder.textInListView.setText(listStorage.get(position).getName());

        if(GlobalVar.getInstance().getArSupported()) {
            if (listStorage.get(position).getActivateAr() && fromDiscover) {
                //Add AR button
                listViewHolder.arInCard.setVisibility(View.VISIBLE);
            } else {
                //Hide AR button
                listViewHolder.arInCard.setVisibility(View.GONE);
            }
        } else {
            listViewHolder.arInCard.setVisibility(View.GONE);
        }

        File dir = context.getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = listStorage.get(position).getImageName() + "." + listStorage.get(position).getExt();
        File mypath = new File(dir,filename);
        listViewHolder.imageInListView.setImageDrawable(Drawable.createFromPath(mypath.toString()));


        listViewHolder.imageInListView.setImageMatrix(Util.scaleTop(listViewHolder.imageInListView, context));

        return convertView;
    }

    static class ViewHolder{
        TextView textInListView;
        ImageView imageInListView;
        ImageView arInCard;
    }
}
