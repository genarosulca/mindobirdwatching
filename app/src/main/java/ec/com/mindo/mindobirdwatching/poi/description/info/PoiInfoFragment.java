package ec.com.mindo.mindobirdwatching.poi.description.info;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.WrappingGridView;
import ec.com.mindo.mindobirdwatching.bird.library.description.BirdLibraryDescriptionActivity;
import ec.com.mindo.mindobirdwatching.db.ConnectionSQLiteHelper;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.entities.PoiVO;
import ec.com.mindo.mindobirdwatching.poi.description.PoiDescriptionActivity;
import ec.com.mindo.mindobirdwatching.util.BirdUtils;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.DBConstants;
import ec.com.mindo.mindobirdwatching.util.GlobalVar;
import ec.com.mindo.mindobirdwatching.util.Util;

public class PoiInfoFragment extends Fragment {

    private List<BirdVO> birdList;
    private Boolean descriptionOpened;
    private Boolean whatFindOpened;
    private Boolean fromDiscover;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedObj = inflater.inflate(R.layout.fragment_poi_info, container, false);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        PoiVO poiObject = (PoiVO) getArguments().getSerializable(PoiDescriptionActivity.SELECTED_POI);
        fromDiscover = getArguments().getBoolean(Constants.FROM_DISCOVER);

        this.getBirdCol(inflatedObj, poiObject);

        initDescription(inflatedObj, poiObject);
        initWhatToFind(inflatedObj);

        return inflatedObj;
    }

    private void initDescription(View inflatedObj, PoiVO poiObject){
        TextView description = inflatedObj.findViewById(R.id.poi_description_descriptionTextView);
        description.setText(poiObject.getDescription());
        FrameLayout descriptionLabel = inflatedObj.findViewById(R.id.poi_description_label_container);
        ImageView descriptionLabelIcon = inflatedObj.findViewById(R.id.poi_description_icon);
        descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
        descriptionOpened = Boolean.TRUE;
        descriptionLabel.setOnClickListener(view -> {
            if(descriptionOpened){
                descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                description.setVisibility(View.GONE);
                descriptionOpened = Boolean.FALSE;
            } else {
                descriptionLabelIcon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                description.setVisibility(View.VISIBLE);
                descriptionOpened = Boolean.TRUE;
            }
        });
    }

    private void initWhatToFind(View inflatedObj){
        WrappingGridView gridview = inflatedObj.findViewById(R.id.poi_info_bird_gridview);
        PoiInfoBirdAdapter customAdapter = new PoiInfoBirdAdapter(inflatedObj.getContext(),
                birdList);
        FrameLayout label = inflatedObj.findViewById(R.id.poi_what_find_label_container);
        ImageView icon = inflatedObj.findViewById(R.id.poi_what_find_icon);
        icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
        whatFindOpened = Boolean.TRUE;
        label.setOnClickListener(view -> {
            if(whatFindOpened){
                icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black);
                gridview.setVisibility(View.GONE);
                whatFindOpened = Boolean.FALSE;
            } else {
                icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                gridview.setVisibility(View.VISIBLE);
                whatFindOpened = Boolean.TRUE;
            }
        });

        gridview.setAdapter(customAdapter);

        gridview.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(view.getContext(), BirdLibraryDescriptionActivity.class);
            BirdVO selectedBird = birdList.get(position);
            selectedBird.setSoundsGallery(BirdUtils.getAllSounds(selectedBird.getBirdId(), inflatedObj.getContext()));
            selectedBird.setVideoGallery(BirdUtils.getAllVideos(selectedBird.getBirdId(), inflatedObj.getContext()));
            selectedBird.setImageGallery(BirdUtils.getAllImages(selectedBird.getBirdId(), inflatedObj.getContext()));

            intent.putExtra("selectedBird", selectedBird);
            intent.putExtra(Constants.FROM_DISCOVER, fromDiscover);

//            intent.putExtra(Constants.FROM_DISCOVER, Boolean.FALSE);
            startActivity(intent);
        });
    }

    private void getBirdCol(View inflatedObj, PoiVO poiObject) {
        ConnectionSQLiteHelper conn = new ConnectionSQLiteHelper(inflatedObj.getContext(),
                DBConstants.getDbName(GlobalVar.getInstance().getCurrentLanguage()), null, 1);
        SQLiteDatabase db = conn.getReadableDatabase();
        String[] parameters = poiObject.getRelatedBirds().split(",");
        Cursor cursor = db.rawQuery("SELECT * FROM " + DBConstants.TABLE_BIRD + " WHERE " +
                DBConstants.FIELD_ID + " IN (" + Util.makePlaceholders(parameters.length) + ")", parameters);
        birdList = new ArrayList<>();
        while (cursor.moveToNext()) {
            birdList.add(
                    new BirdVO(
                            cursor.getString(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getInt(4) != 0,
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getString(7),
                            cursor.getString(8),
                            cursor.getString(9),
                            cursor.getString(10),
                            cursor.getString(11),
                            cursor.getString(12),
                            cursor.getInt(13) != 0
                    )
            );
        }
        cursor.close();
        db.close();
        conn.close();
    }
}
