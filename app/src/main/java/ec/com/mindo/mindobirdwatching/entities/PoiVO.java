package ec.com.mindo.mindobirdwatching.entities;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PoiVO implements Serializable {

    private String poiId;
    private String imageName;
    private String ext;
    private Double latitude;
    private Double longitude;
    private String title;
    private String subTitle;
    private String description;
    private String relatedBirds;
    private List<BirdVO> birdCol;

    private float distance;

    public PoiVO(String poiId, String imageName, String ext, Double latitude, Double longitude, String title, String subTitle, String description, String relatedBirds) {
        this.poiId = poiId;
        this.imageName = imageName;
        this.ext = ext;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.subTitle = subTitle;
        this.description = description;
        this.relatedBirds = relatedBirds;
    }

}
