package ec.com.mindo.mindobirdwatching.bird.library.description;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import ec.com.mindo.mindobirdwatching.BaseActivity;
import ec.com.mindo.mindobirdwatching.R;
import ec.com.mindo.mindobirdwatching.bird.library.description.ar.ARAnimationActivity;
import ec.com.mindo.mindobirdwatching.bird.library.description.ar.ARPlaneDetectionActivity;
import ec.com.mindo.mindobirdwatching.bird.library.description.bio.BirdLibraryBioFragment;
import ec.com.mindo.mindobirdwatching.bird.library.description.danger.BirdLibraryDangerFragment;
import ec.com.mindo.mindobirdwatching.bird.library.description.images.BirdLibraryImagesFragment;
import ec.com.mindo.mindobirdwatching.bird.library.description.sounds.BirdLibrarySoundsFragment;
import ec.com.mindo.mindobirdwatching.bird.library.description.videos.BirdLibraryVideosFragment;
import ec.com.mindo.mindobirdwatching.entities.BirdVO;
import ec.com.mindo.mindobirdwatching.util.Constants;
import ec.com.mindo.mindobirdwatching.util.Util;

public class BirdLibraryDescriptionActivity extends BaseActivity {

    public static final String SELECTED_BIRD = "selectedBird";

    private Boolean withAR = Boolean.FALSE;
    private BirdVO selectedBird;

    private BottomNavigationView bottomNavigationView;
    private FrameLayout frameLayout;

    private BirdLibraryBioFragment bioFragment;
    private BirdLibraryImagesFragment imagesFragment;
    private BirdLibraryVideosFragment videosFragment;
    private BirdLibrarySoundsFragment soundsFragment;
    private BirdLibraryDangerFragment inDangerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = getLayoutInflater();
        inflater.inflate(R.layout.activity_birdlibrary_description_main, (ViewGroup) findViewById(R.id.container));

        frameLayout = (FrameLayout) findViewById(R.id.fragmet_container);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav);

        // Obtengo el objeto de intent
        selectedBird = (BirdVO) getIntent().getExtras().getSerializable(SELECTED_BIRD);
        boolean fromDiscover = getIntent().getExtras().getBoolean(Constants.FROM_DISCOVER);


        // Creo bundle para enviar a fragment
        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_BIRD, selectedBird);
        bundle.putBoolean(Constants.FROM_DISCOVER, fromDiscover);

        bioFragment = new BirdLibraryBioFragment();
        bioFragment.setArguments(bundle);

        imagesFragment = new BirdLibraryImagesFragment();
        imagesFragment.setArguments(bundle);

        videosFragment = new BirdLibraryVideosFragment();
        videosFragment.setArguments(bundle);

        soundsFragment = new BirdLibrarySoundsFragment();
        soundsFragment.setArguments(bundle);

        inDangerFragment = new BirdLibraryDangerFragment();
        inDangerFragment.setArguments(bundle);

        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmet_container,
                    bioFragment).commit();
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.nav_bio:
                    setFragment(bioFragment);
                    return true;
                case R.id.nav_images:
                    setFragment(imagesFragment);
                    return true;
                case R.id.nav_videos:
                    setFragment(videosFragment);
                    return true;
                case R.id.nav_sound:
                    setFragment(soundsFragment);
                    return true;
                case R.id.nav_ar:
                    iniciarAr();
                default:
                    return false;
            }
        });

        if(fromDiscover){
            headerText.setText(R.string.scan_hotspot);
        } else {
            headerText.setText(R.string.bird_library);
        }

        ImageView imageView = findViewById(R.id.bird_title_background);

        File dir = this.getDir(Constants.I_BIRDS_FOLDER, Context.MODE_PRIVATE); //Creating an internal dir;
        String filename = selectedBird.getImageName() + "." + selectedBird.getExt();
        File mypath = new File(dir,filename);
        imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));

        imageView.setImageMatrix(Util.scaleTop(imageView, this));

        TextView title = findViewById(R.id.bird_title_txt);
        title.setText(selectedBird.getName());
        //loadingScreen.setVisibility(View.GONE);
    }

    private void setFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmet_container, fragment)
                .commit();
    }

    private void iniciarAr(){
        //loadingScreen.setVisibility(View.VISIBLE);
        Intent intent = new Intent(this, ARAnimationActivity.class);
        intent.putExtra("selectedBird", selectedBird);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //loadingScreen.setVisibility(View.GONE);
    }
}
