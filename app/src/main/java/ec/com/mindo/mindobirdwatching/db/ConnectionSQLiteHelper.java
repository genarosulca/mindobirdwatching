package ec.com.mindo.mindobirdwatching.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import ec.com.mindo.mindobirdwatching.util.DBConstants;

public class ConnectionSQLiteHelper extends SQLiteOpenHelper {



    public ConnectionSQLiteHelper(@Nullable Context context, @Nullable String name,
                                  @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Actualiza version
//        db.execSQL("DROP TABLE IF EXISTS poiMainObject");
//        onCreate(db);
    }
}
